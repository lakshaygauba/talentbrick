# TalentBrick 
Say hello to the Open-Source education model. Learning made easy without ADS and trackers, Clear concepts at a glance, and Get access to quality study materials only on TalentBrick.

### [Start Learning](https://www.talentbrick.com)

## License
TalentBrick is open-source software licensed under the [![](https://licensebuttons.net/l/by-nc-nd/4.0/88x31.png)](https://creativecommons.org/licenses/by-nc-nd/4.0/)