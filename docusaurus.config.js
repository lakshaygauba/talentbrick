// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

/** @type {import('@docusaurus/types').Config} */
const config = {
  stylesheets: [
    {
      rel: "icon",
      href: "/img/docusaurus.png",
    },
    {
      rel: "apple-touch-icon",
      href: "https://icdn.talentbrick.com/main/icons/apple-touch-icon.png",
    },
  ],
  title: "TalentBrick",
  tagline:
    "Say hello to the Open-Source education model. Learning made easy without ADS and trackers, Clear concepts at a glance, and Get access to quality study materials only on TalentBrick.",
  url: "https://www.talentbrick.com",
  baseUrl: "/",
  trailingSlash: false,
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",
  organizationName: "talentbrick", // Usually your GitHub org/user name.
  projectName: "talentbrick", // Usually your repo name.

  presets: [
    [
      "@docusaurus/preset-classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          routeBasePath: "/",
          sidebarPath: require.resolve("./sidebars.js"),
          editUrl: "https://gitlab.com/talentbrick/talentbrick/-/blob/main/",
          showLastUpdateAuthor: false,
          showLastUpdateTime: false,
        },
        blog: {
          path: "./blog",
          routeBasePath: "/blog",
          blogSidebarCount: 0,
          feedOptions: {
            type: null,
            copyright: `Copyright © ${new Date().getFullYear()} | Talent Brick | All Rights Reserved`,
          },
          showReadingTime: true,
          // Please change this to your repo.
          editUrl: "https://gitlab.com/talentbrick/talentbrick/-/blob/main/",
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      announcementBar: {
        id: 'support_us',
        content:
          'Do you want to discuss something? Have you got any doubt? Say no more; <a target="_blank" rel="noopener noreferrer" href="https://ask.talentbrick.com/composer"><b>Head to our Forum</b></a> and let us be your 🪂 troubleshooters. <br> 🙁 is how students approach our <a target="_blank" rel="noopener noreferrer" href="https://ask.talentbrick.com/composer"><b>forum</b></a> and 🙂 is how they return 😉',
        backgroundColor: '#0a1746',
        textColor: '#f5f6f7',
        isCloseable: true,
      },
      colorMode: {
        defaultMode: "dark",
        disableSwitch: false,
        respectPrefersColorScheme: false,
      },
      hideableSidebar: true,
      navbar: {
        title: "Talent Brick",
        hideOnScroll: true,
        logo: {
          alt: "TalentBrick",
          src: "https://icdn.talentbrick.com/main/icons/logo.svg",
        },
        items: [
          {
            type: "dropdown",
            label: "MCQ Questions",
            position: "left",
            items: [
              {
                label: "Class 11",
                to: "/ncert/class-11/mcq",
              },
              {
                label: "Class 12",
                to: "/ncert/class-12/mcq",
              },
            ],
          },
          {
            to: "/blog",
            label: "Blog",
            position: "left",
          },
          {
            to: "https://ask.talentbrick.com/",
            label: "Forum",
            position: "left",
            target: "_self",
          },
          {
            to: "https://ask.talentbrick.com/contact-us",
            label: "Write Blog",
            position: "right",
            activeBaseRegex: "docs/(next|v8)",
            target: "_self",
          },
          {
            label: "Donate",
            to: "/donate",
            position: "right",
          },
          {
            label: "Sign Up",
            to: "https://ask.talentbrick.com/signup",
            position: "right",
            activeBaseRegex: "docs/(next|v8)",
            target: "_self",
          },
          {
            label: "Log In",
            to: "https://ask.talentbrick.com/login",
            position: "right",
            activeBaseRegex: "docs/(next|v8)",
            target: "_self",
          },
        ],
      },
      footer: {
        style: "dark",
        links: [
          {
            title: "Resources",
            items: [
              {
                label: "Ask Doubt",
                href: "https://ask.talentbrick.com/composer",
              },
              {
                label: "MCQ Questions",
                to: "/ncert/mcq",
              },
              {
                label: "Watch Videos (YouTube)",
                href: "https://go.talentbrick.com/yt",
              },
              {
                label: "NCERT Books",
                to: "/ncert/books",
              },
            ],
          },
          {
            title: "Community",
            items: [
              {
                label: "Discord",
                href: "https://discord.gg/pqSDdW3tuM",
              },
              {
                label: "Twitter",
                href: "https://twitter.com/talentbrick",
              },
              {
                label: "Facebook",
                href: "https://fb.com/talentbrick",
              },
              {
                label: "Instagram",
                href: "https://instagram.com/talent_brick",
              },
            ],
          },
          {
            title: "More",
            items: [
              {
                label: "Blog",
                to: "/blog",
              },
              {
                label: "GitLab",
                href: "https://gitlab.com/talentbrick/",
              },
              {
                label: "GitHub",
                href: "https://github.com/talentbrick/",
              },
              {
                label: "Privacy Policy",
                href: "/privacy-policy",
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} · <a href="https://www.talentbrick.com/" alt="TalentBrick">Talent Brick</a> · All Rights Reserved`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
