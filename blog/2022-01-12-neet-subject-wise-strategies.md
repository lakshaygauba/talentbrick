---
title: NEET Subject-Wise Strategies
authors: anshul
slug: neet-subject-wise-strategies
tags: [strategy, subject-wise, tricks, tips]
image: https://icdn.talentbrick.com/main/article-anshul.png
---

Before coming to any particular subject, let me make clear some common points to be followed step-wise on every day of the preparation to ace the exams like NEET and JEE-Mains:
<!--truncate-->
1. Listen with full concentration in class
2. Go home and revise the work.
3. Be consistent in doing so during all the years of preparation.
4. Do smart work along with hard work. (Hard work only refers to your sitting hours but bringing maximum output from those hours is something which makes you a smart worker)
5. Be a good time manager that can be achieved with practice.
6. Try to set your bio-clock. (Solve tests and questions from 2 PM TO 5 PM daily as the time of NEET exam is 2 to 5. Avoid sleep, food, water, washroom in that duration until or unless an emergency comes up)
7. Doubts should be cleared side by side by the teachers or other sources without any backlog.
8. Any test you take must be followed by the analysis of the mistakes and doubt clearance.
9. Make short notes for quick revision.
10. Solve PYQ's for all the subjects.

:::note
It is not necessary that a person who is very intelligent or intellectual aces this exam but the one, with minimum required level of intelligence, who follows the above directions will surely get success.
:::

Now, after this basic makeover, we are ready to migrate ourselves to the subject-wise plans:-

## PHYSICS
Physics is conceptual with a minimum amount of cramming required. The following points can be followed:-
1. Along with full concentration in class, take proper notes of the topic.
2. Revision of the work is so important in physics that if not focussed upon, can lead to critical setbacks later.
3. In most of the institutions, proper notes are being given for physics, keeping the primary focus on notes, following some other sources like NCERT, coaching study packages content, other reference books' content, etc. as and when required.
4. All the questions in your package or any other book you use to practice must be solved without any backlog either daily or after the completion of one chapter (average 300-500 questions for each chapter).
5. Use back revision strategy (Suppose you take a test and find a question from the theory that you never witnessed before, after the test, go and read that topic from NCERT, Coaching module, or any side book and ensure that you make yourself a master of that topic for the upcoming tests)
6. Try to make formula charts as formula remembrance is very important.


## CHEMISTRY

### Physical Chemistry
Point 1,2,4,5,6 of physics is to be followed here also.
3rd point is being modified as:
The primary focus should be on notes as well as NCERT (At least read once or more if notes are not so much prominent).

### Organic Chemistry
Point 1,2,4,5 of physics is to be followed here also.
Modified 3rd point from physical chemistry to be followed here also.
Others:
a. Have faith in the teacher and follow all his/her instructions blindly.
b. Do not ever miss a class in organic as the topics are extremely to each other.
c. Do not ever miss to revise the work at home for the same reason being told in point 'b'.
d. Focus as much as you can, on the reaction mechanisms.
e. Make reaction charts for quick revision.

### Inorganic Chemistry
Point 5 from Physics is to be followed here also.
Others:
1. Like biology, NCERT is the foremost thing. Read as many times as possible along with a proper concentration in class and revision back home.
2. Make short notes of the topics which are hard to memorize for quick revision.
3. Make out tricks (mnemonics) or find the same from youtube to learn cramming parts and trends.
4. Only PYQ'S if solved properly will surely prove to be substantial.


## BIOLOGY:
Point 5 of physics and points 1,2,3 of inorganic chemistry are to be followed here also.
Others:
1. Along with PYQ'S, some books like [MTG-NCERT AT YOUR FINGERTIPS](https://amzn.to/31Kjrqs) can be solved.
2. Some topics are compulsorily required to be read from outside NCERT to completely understand the text of NCERT. While doing that try to incorporate the 
out-of-ncert findings in the NCERT itself at the empty spaces on the sides of all the pages.
3. Diagram knowledge proves to be substantial in biology.

:::note
Above things are subject to any kind of changes as per the prevailing conditions of a particular school or coaching or student with a basic plot remaining the same.
:::

Wishing you all the very best
Thank You 
Anshul Rana
Warm Regards.
