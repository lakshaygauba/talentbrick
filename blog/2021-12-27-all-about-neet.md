---
title: All About NEET
authors: pavitra
tags: [experience, strategy, story, tips, time management, exclusive, NEET]
slug: about-neet
image: https://icdn.talentbrick.com/main/article-pavitra.png
---

## Experience During Neet Exam
My reporting time was 11 a.m , so I had a waiting time of about 3 hrs before my paper would actually start. In these three hours instead of thinking about subjects or making myself pressurized , I motivated myself, kept myself calm, and made myself ready for any type of paper to come. So paper started at dot 2 pm, I filled up my personal details in the OMR, then I did my paper. After the paper I was a bit nervous, as there were a few questions I was doubtful about but after checking from answer keys released by various coachings I was satisfied with my performance.

<!--truncate-->

## Time Management During Exam
As everyone has their own strategy to solve the paper, so did I. My strength was physics and my weak end was chemistry. I used to solve 20 questions of physics in the first 30 min, then biology in the next 25 min, the rest 25 questions of physics in 35min, the next 30 min I used to give chemistry. The left 1 hr was for revision and OMR filling. I followed the same in my NEET exam.

## Time Management During Preparation
I wasn’t very good at the timetables part but I had my weekly agendas. Every week I wrote down what books and chapters I have to read the entire week and made sure I completed it by the end of the week. This was helpful for me as if some days I am not productive, the other days I can work harder and still be on my schedule and was less pressurized per day. Additionally, I made a mistake in my 11th of sleeping less, it only negatively affected my productivity and my learning strength and also made me anxious. In 12th I made my 7 hrs to 8 hrs sleep per day a compulsion and it helped me a lot. Also, realize it’s a 2 yrs journey, study hard but enjoy as well, give yourself breaks. Talking of the subjects, I used to give more time to chemistry then biology and last to physics. It all depends on what is your weakness, mine was chemistry. 

## How To Prepare
### Biology
Talking of biology, we know it’s half the NEET paper and is the part where most of the students are good at, so what can we do to be ready for it? Everyone knows questions come from [NCERT](https://amzn.to/3snBuO4), many read [NCERT](https://amzn.to/3snBuO4), some read it twice and thrice and very few have actually read it multiple times. So there is no such times, but read it as many times you can. Everytime you find new lines underline them, follow a color scheme if you can and most important write down new points on side of the pages for last minute revision. But at the same time its equally important to refer notes of your teachers and coaching books as in my year’s paper two three questions were out of [NCERT](https://amzn.to/3snBuO4).

### Chemistry
For chemistry I followed [NCERT](https://amzn.to/3slS3tP), notes, and some private publishers’ books like [shree balaji publications](https://amzn.to/3mlFCdO), etc. After studying the whole 11th and 12th I realized that never go for books which you think are not for you, you are just confusing yourself with exceptions. Chemistry is full of exceptions, you can’t learn all of them so learn a few of them only which your teacher tells or you have read in NCERT and PYQs. For inorganic follow [NCERT](https://amzn.to/3slS3tP), for organic follow [NCERT](https://amzn.to/3slS3tP) and also solve questions from many resource books like [MS Chauhan](https://amzn.to/3Fjvolq), for physical chemistry its more practice and less learning so, I read from NCERT but practiced from question books of my institute as well as [N. Avasthi](https://amzn.to/3snYDjl).

### Physics
We know physics is more of practice and less of learning. I followed [NCERT](https://amzn.to/3Ff2XW0), notes, and [H.C Verma](https://amzn.to/3ebyc8l). Solve as many questions as you can. **Neet also focuses on the subjective parts of NCERT especially part 2 of both 11th and 12th**, so read it carefully. If you are struggling with any portion of a chapter, read that part from NCERT, read it from notes and then solve questions from that part and correct your mistakes and you are all good. As most of the NEET aspirants struggle in this part, if you work hard to excel in this portion you are putting yourself ahead of the majority. If you think you don’t know physics you would never understand physics but if you work upon why I don’t then you would realize how easy and enjoyable it can be.

As for all the subjects, **NEET is more of an accuracy-based paper, practicing as many questions as you can is the key for all the subjects**. Work on concepts but devote time to making yourself accustomed to the paper, solving PYQs, and practicing what the question setter actually wants you to answer.

At last, it’s all about learning, you learn new things every day, you are getting better every day.  
Happy learning!!  
Thank you  
Pavitra Goyal
