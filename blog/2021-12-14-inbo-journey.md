---
title: INBO Journey
authors: divyansh
slug: inbo-journey
tags: [experience, story, time management, INBO]
---

## Introduction
I am Divyansh Soni. I have secured **AIR 10** in INBO. It would be a great pleasure to write here about how I have prepared for it and what all books are recommended for it.

<!--truncate-->

## How I Prepared
I have solved all the last 5 years' papers of the National Standard Examination in Biology and Indian National Biology Olympiad (INBO) although the questions seemed challenging for me. I have solved all the questions that I can solve based on my knowledge gained during my regular preparation of NEET. The reason for these questions being challenging is that they are directly picked up from the research fields. Also, there were questions that I was unable to solve.

## Books I Preferred
I read them from specific books such as [Campbell](https://amzn.to/3oUSUzH) and [Robert J. Brooker](https://amzn.to/3p12roZ) , which is a renowned book on genetics. I think there is no need to go deeper into it. The only need is some topics related to previous year questions. Also do not forget to learn the most important topic, Ethology from [Campbell](https://amzn.to/3oUSUzH). There were many questions related to this book. I enjoyed this topic the most while studying. 

## Time Management
Speaking of time management you must utilize your time wisely during the paper. There is a strong need to evade some questions that are time consuming , but remember you must read all the questions because many times long questions are easy ones which are just giveaways. Try to solve questions as fast as possible . By going through last year's papers, I learned the special tactics and tricks to solve the questions which also saved my time during the paper. Also, I got to know about the patterns of questions asked. At last I would like to thank you.
