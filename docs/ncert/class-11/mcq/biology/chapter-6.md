---
sidebar_position: 6
title: MCQ Questions Anatomy of Flowering Plants Chapter 6 Class 11 Biology
description: Anatomy of Flowering Plants Chapter 6 Biology Class 11, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-11-bio-chapter-6.png
sidebar_label: Chapter 6
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

**NCERT** Biology Class 11th Ch.6 Anatomy of Flowering Plants MCQ Practice Questions for NEET | Class 11.

Prepare these **important MCQ Questions** of Biology Class 11 Anatomy of Flowering Plants Chapter 6, Latest questions to expected to come in **NEET | School Exams**.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. During the formation of primary plant body specific regions of apical meristem produce __
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Dermal tissues</label>
    <br />
    <input name="question1" type="radio" />
    <label>2.Ground tissues</label><br />
    <input name="question1" type="radio" />
    <label>3. Vascular tissues</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">4. All of the these</label>
  </div><hr />

### 2. Choose the incorrect match with respect to the following diagram.
![Choose the incorrect match with respect to the following diagram.](https://icdn.talentbrick.com/Static/Choose%20the%20incorrect%20match%20with%20respect%20to%20the%20following%20diagram.png)
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. A: Central cylinder</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">2. B: Protoderm</label><br />
    <input name="question2" type="radio" />
    <label>3. C: Root cap</label><br />
    <input name="question2" type="radio" />
    <label>4. D: Root Apical meristem</label>
  </div><hr />

 ### 3. Choose the Odd one out with respect to cylindrical meristems.
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. Intrafascicular meristem</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">2. Intercalary meristem</label><br />
    <input name="question3" type="radio" />
    <label>3. Interfascicular meristem</label><br />
    <input name="question3" type="radio" />
    <label>4. Cork cambium</label>
  </div><hr />

### 4. Choose the correct statement with respect to collenchyma.
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. They occur below endodermis in most of the dicots.</label><br />
    <input name="question4" type="radio" />
    <label>2. They are either closely packed or have small intercellular spaces.</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">3. They often contain chloroplasts.</label><br />
    <input name="question4" type="radio" />
    <label>4. Corners of these cells are thickened due to deposition of cutin.</label>
  </div><hr />

 ### 5. Choose the correct statement for sclerenchyma.
  <div className="tb-mcq">
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">1. On the basis of structure, origin and development scelerchyma may be either sclereids or fibres.</label><br />
    <input name="question5" type="radio" />
    <label>2. They are commonly found in fruit wall of guava.</label><br />
    <input name="question5" type="radio" />
    <label>3. Fibres do not occur in groups, in various parts of plants.</label><br />
    <input name="question5" type="radio" />
    <label>4. They provide mechanical support to young stems and petiole.</label>
  </div><hr />

### 6. Thick walled bundle sheath cells are found in:
  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. Monocot leaf</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">2. Dicot leaf</label><br />
    <input name="question6" type="radio" />
    <label>3. Monocot roots</label><br />
    <input name="question6" type="radio" />
    <label>4. Dicot roots</label>
  </div><hr />

### 7. Thick walled parenchyma cells are found in:
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. Monocot root </label><br />
    <input name="question7" type="radio" />
    <label>2. Dicot stem pericycle</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">3. Dicot root pericycle</label><br />
    <input name="question7" type="radio" />
    <label>4. Dicot stem cortex</label>
  </div><hr />

### 8. Intiation of vascular cambium during secondary growth in roots takes place in which cells?
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. Conjuctive tissue</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">2. Pericycle</label><br />
    <input name="question8" type="radio" />
    <label>3. Phloem</label><br />
    <input name="question8" type="radio" />
    <label>4. Cortex</label>
  </div><hr />

### 9. Correcy description about phellogen.
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. It is single layered,thin walled with oval cells.</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">2. It is rectangular, and made up of two layers.</label><br />
    <input name="question9" type="radio" />
    <label>3. It cuts off cells only on inner side.</label><br />
    <input name="question9" type="radio" />
    <label>4. It develops in stelar region of dicot stem.</label>
  </div><hr />

### 10. Choose the incorrect match among the following.
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Lenticels : Lens shaped</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">2. Radical conduction : Conjuctive tissue</label><br />
    <input name="question10" type="radio" />
    <label>3. Xylem parenchyma : Fats and Starch</label><br />
    <input name="question10" type="radio" />
    <label>4. Sieve tube elements : Peripheral cytoplasm</label>
  </div><hr />

### 11. Identify the tissue/group of cells from the following information.
i. Elongated, tapering, cylindrical.  
ii. It has pits through which plasmodesmatal connection exist between cells.
  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. Xylem parenchyma</label><br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">2. Phloem parenchyma</label><br />
    <input name="question11" type="radio" />
    <label>3. Xylem fibres</label><br />
    <input name="question11" type="radio" />
    <label>4. Phloem fibres</label>
  </div><hr />

### 12. Spring wood and autumn wood are seen in:
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. Dicot stem, tropical region</label><br />
    <input name="question12" type="radio" />
    <label>2. Dicot root, temperate region</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">3. Dicot stem, temperate region region</label><br />
    <input name="question12" type="radio" />
    <label>4. Monocot stem, temperate region</label>
  </div><hr />

### 13. State True or False
i. Monocots and dicots differ in type, number and location of vascular bundles.  
ii. There are different type of wood on the basis of their time of production.  
iii. Ground tissue is divided into three zones namely hypodermis, cortex and endodermis.
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. T,T,T</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">2. T,T,F</label><br />
    <input name="question13" type="radio" />
    <label>3. F,T,F</label><br />
    <input name="question13" type="radio" />
    <label>4. F,F,T</label>
  </div><hr />

### 14. Choose the incorrect option for bast fibres.
  <div className="tb-mcq">
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">1. They are generally absent in primary phloem.</label><br />
    <input name="question14" type="radio" />
    <label>2. They are much elongated and branched.</label><br />
    <input name="question14" type="radio" />
    <label>3. Phloem fibres of flax and hemp are used commercailly.</label><br />
    <input name="question14" type="radio" />
    <label>4. At maturity, they loose their protoplasm.</label>
  </div><hr />

### 15. Choose the incorrect statement.
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Trichome is usually multicellular.</label><br />
    <input name="question15" type="radio" />
    <label>2. Subsidiary cells are specialised in their shape and size.</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">3. Stomatal appartus is comprises of guard cells, stomatal aperture and complimentary cells.</label><br />
    <input name="question15" type="radio" />
    <label>4. Epidermal cells have small amount of cytoplasm lining the cell wall.</label>
  </div>