---
sidebar_position: 7
title: MCQ Questions Structural Organization in Animals Class 11 Chapter 7
description: Structural Organization in Animals Chapter 7 Biology Class 11, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-11-bio-chapter-7.png
sidebar_label: Chapter 7
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

**NCERT** Biology Class 11th Ch.7 Structural Organization in Animals MCQ Practice Questions for NEET | Class 11.

Prepare these **important MCQ Questions** of Biology Class 11 Structural Organization in Animals Chapter 7, Latest questions to expected to come in **NEET | School Exams**.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. Cockroach size ranges from:
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. 34 - 53 cm</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">2. 0.6 – 7.6 cm</label><br />
    <input name="question1" type="radio" />
    <label>3. 0.6 – 7.6 inches</label><br />
    <input name="question1" type="radio" />
    <label>4. 34-53 inches</label></div><hr />

### 2. Neck of cockroach is extension of:
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Head</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">2. Prothorax</label><br />
    <input name="question2" type="radio" />
    <label>3. Abdomen</label><br />
    <input name="question2" type="radio" />
    <label>4. Metathorax</label></div><hr />

### 3. Head of cockroach is formed by fusion of:
  <div className="tb-mcq">
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">1. Six segments</label><br />
    <input name="question3" type="radio" />
    <label>2. Seven segments</label><br />
    <input name="question3" type="radio" />
    <label>3. Ten segments</label><br />
    <input name="question3" type="radio" />
    <label>4. Two segments</label></div><hr />

### 4. Which of the following feature can be used to distinguish male cockroach from female cockroach?
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. Presence of anal cerci</label><br />
    <input name="question4" type="radio" />
    <label>2. Absence of anal cerci</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">3. Wings extend beyond the tip of abdomen</label><br />
    <input name="question4" type="radio" />
    <label>4. Presence of arthrodial membrane</label></div><hr />

### 5. Choose the correct statement.
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Hypopharynx is median inflexible lobe that act as tongue.</label><br />
    <input name="question5" type="radio" />
    <label>2. In females abdomen has 9 segments only.</label><br />
    <input name="question5" type="radio" />
    <label>3. Prothorax does not bears pair of walking legs.</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">4. Neck shows great mobility in all directions.</label></div><hr />

### 6. Correct description of Tegmina is
  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. Opaque, membranous, mesothoracic.</label><br />
    <input name="question6" type="radio" />
    <label>2. Transparent, membranous, metathoracic.</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">3.Opaque, Leathery, mesothoracic.</label><br />
    <input name="question6" type="radio" />
    <label>4. Transparent, Leathery, metathoracic.</label></div><hr />

### 7. How many of the following are part of female reproductive system in cockroach?
| Phallic gland, Collateral gland, Spermathecal pores, Spermatheca, Vestibulum, Titillator.|
|-----------------------------|
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>a. 2</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">b. 4</label><br />
    <input name="question7" type="radio" />
    <label>c. 5</label><br />
    <input name="question7" type="radio" />
    <label>d. 3</label></div><hr />

### 8. Male bears thread like anal styles on which segment:
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. 9th terga</label><br />
    <input name="question8" type="radio" />
    <label>2. 10th sterna</label><br />
    <input name="question8" type="radio" />
    <label>3. 10th terga</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">4. 9th sterna</label></div><hr />

### 9. Compound eyes are situated on:
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Ventral surface</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">2. Dorsal surface</label><br />
    <input name="question9" type="radio" />
    <label>3. Ventro-lateral surface</label><br />
    <input name="question9" type="radio" />
    <label>4. Terminal surface</label></div><hr />

### 10. Choose the correct statement with respect to cockroach.
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>a. In females 7th tergite is boat shaped.</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">b. Blood from sinuses enter heart through ostia.</label><br />
    <input name="question10" type="radio" />
    <label>c. 10 spiracles are present on lateral surface of the body.</label><br />
    <input name="question10" type="radio" />
    <label>d. All of these.</label></div><hr />

### 11. Grinding and incising region are located on which mouth part of cockroach.
  <div className="tb-mcq">
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">1. Mandible</label><br />
    <input name="question11" type="radio" />
    <label>2. Maxillae</label><br />
    <input name="question11" type="radio" />
    <label>3. Hypopharynx</label><br />
    <input name="question11" type="radio" />
    <label>4. Labium</label></div><hr />

### 12. Choose the incorrect statement with respect to cockroach.
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. Female cockroach consists of a pair of spermatheca.</label><br />
    <input name="question12" type="radio" />
    <label>2. On average female lays 9-10 ootheca.</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">3. Foregut is not lined by cuticle.</label><br />
    <input name="question12" type="radio" />
    <label>4. Hindgut is broader than midgut.</label></div><hr />

### 13. Correct description regarding salivary gland in cockroach.
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. A pair present near gizzard.</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">2. A pair present near crop.</label><br />
    <input name="question13" type="radio" />
    <label>3. Two pairs present near crop.</label><br />
    <input name="question13" type="radio" />
    <label>4. Two pairs present near gizzard.</label></div><hr />

### 14. Choose the correct statement for digestive system of cockroach.
  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>1. At the junction of midgut and foregut 100-150 tubules of gastric caeca are present.</label><br />
    <input name="question14" type="radio" />
    <label>2. At the junction of midgut and hindgut 6-8 tubules of gastric caeca are present.</label><br />
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">3. At the junction of midgut and hindgut 100-150 yellow filamentous tubules are present.</label><br />
    <input name="question14" type="radio" />
    <label>4. At the junction of midgut and hindgut 6-8 yellow filamentous tubules are present.</label></div><hr />

### 15. Blood flow in cockroach is:
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Always from anterior to posterior.</label><br />
    <input name="question15" type="radio" />
    <label>2. Always towards abdomen.</label><br />
    <input name="question15" type="radio" />
    <label>3. Sometimes posterior to anterior and sometimes anterior to posterior.</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">4. Always from posterior to anterior.</label></div><hr />

### 16. Malpighian tubules consist of:
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. Glandular cells</label><br />
    <input name="question16" type="radio" />
    <label>2. Cuboidal cells</label><br />
    <input name="question16" type="radio" />
    <label>3. Ciliated cells</label><br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">4. Both 1 and 3</label></div><hr />

### 17. In earthworm, which condition is seen to prevent self fertilizations?
  <div className="tb-mcq">
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">1. Protandrous</label><br />
    <input name="question17" type="radio" />
    <label>2. Progynous.</label><br />
    <input name="question17" type="radio" />
    <label>3. Different position of reproductive structures.</label><br />
    <input name="question17" type="radio" />
    <label>4. May be 1 or 2.</label></div><hr />

### 18. How many spermathecae are found in earthworm?
  <div className="tb-mcq">
    <input name="question18" type="radio" />
    <label>a. 4</label><br />
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">b. 8</label><br />
    <input name="question18" type="radio" />
    <label>c. 2</label><br />
    <input name="question18" type="radio" />
    <label>d. 10</label></div><hr />

### 19. If head of cockroach is cut off, it will survive for as long as:
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. One minute</label><br />
    <input name="question19" type="radio" />
    <label>2. One day</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">3. One week</label><br />
    <input name="question19" type="radio" />
    <label>4. One month</label></div><hr />

### 20. Choose the correct statement:
A. Male gonapophysis is chitinous symmetrical structure.  
B. Each ovary formed of 8 ovarioles
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1. Both A and B are incorrect</label><br />
    <input name="question20" type="radio" />
    <label>2.Both A and B are correct</label><br />
    <input name="question20" type="radio" />
    <label>3. Only A is correct</label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">4. Only B is correct</label></div><hr />

### 21. Correct statement with respect to frog.
  <div className="tb-mcq">
    <input name="question21" type="radio" />
    <label>1.Out of sense organs eye and internal ears are cellular aggregation around nerve endings and rest are well organised structure.</label><br />
    <input name="question21" type="radio" />
    <label>2. Female lays 250-300 eggs at a time.</label><br />
    <input id="q21" name="question21" type="radio" />
    <label htmlFor="q21">3. Midbrain is characterized by a pair of optic lobes.</label><br />
    <input name="question21" type="radio" />
    <label>4. The ventricle opens into sac like conus arteriosus on dorsal side of heart.</label></div><hr />

### 22. In earthworm on average each cocoon produces __ baby worms.
  <div className="tb-mcq">
    <input name="question22" type="radio" />
    <label>a. 20</label><br />
    <input name="question22" type="radio" />
    <label>b. 2</label><br />
    <input id="q22" name="question22" type="radio" />
    <label htmlFor="q22">c. 4</label><br />
    <input name="question22" type="radio" />
    <label>d. 9</label></div><hr />

### 23. Earthworm does not have:
  <div className="tb-mcq">
    <input name="question23" type="radio" />
    <label>1. Eyes</label><br />
    <input name="question23" type="radio" />
    <label>2. Larval stages</label><br />
    <input name="question23" type="radio" />
    <label>3. Specialized breathing devices</label><br />
    <input id="q23" name="question23" type="radio" />
    <label htmlFor="q23">4. All of these</label></div><hr />

### 24. Choose the correct with respect to morphology of earthworm.
  <div className="tb-mcq">
    <input name="question24" type="radio" />
    <label>1. Setae are absent on first and last segment only.</label><br />
    <input name="question24" type="radio" />
    <label>2. Four spermathecal aperture are present on ventro lateral side.</label><br />
    <input id="q24" name="question24" type="radio" />
    <label htmlFor="q24">3. Integumentary nephridia opens on body surface.</label><br />
    <input name="question24" type="radio" />
    <label>4. Peristomium acts as a wedge to force open crack in soil.</label></div><hr />

### 25. Choose the incorrect match for earthworm.
  <div className="tb-mcq">
    <input name="question25" type="radio" />
    <label>1. Typholosole – begins from 26th segment</label><br />
    <input id="q25" name="question25" type="radio" />
    <label htmlFor="q25">2. Gizzard – 5-7 segments</label><br />
    <input name="question25" type="radio" />
    <label>3. Blood glands – 4,5,6 segments</label><br />
    <input name="question25" type="radio" />
    <label>4. Buccal segment – 1st segment</label></div>