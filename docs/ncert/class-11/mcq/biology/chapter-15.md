---
sidebar_position: 15
title: MCQ Plant Growth and Development Chapter 15 Biology Class 11
description: Plant Growth and Development Chapter 15 Biology Class 11, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-11-bio-chapter-15.jpg
sidebar_label: Chapter 15
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

NCERT Biology Class 11th Ch.15 Plant Growth and Development MCQ Practice Questions for NEET | Class 11

Prepare these important MCQ Questions of Biology Class 11 Plant Growth and Development Chapter 15, Latest questions to expected to come in NEET | School Exams.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. First step in process of plant growth is:
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Zygote formation</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">2. Seed germination</label><br />
    <input name="question1" type="radio" />
    <label>3. Periclinal division</label><br />
    <input name="question1" type="radio" />
    <label>4. Anticlinal division</label></div><hr />

### 2. Choose the incorrect statement.
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Development is sum of two processes growth and differentiation.</label><br />
    <input name="question2" type="radio" />
    <label>2. Both growth and differentiation are open in higher plants.</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">3. Increase in length denotes the growth in dorsiventral leaf.</label><br />
    <input name="question2" type="radio" />
    <label>4. Cork cambium appear later in life in gymnosperms.</label></div><hr />

### 3. Choose the incorrect match.
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. F.W. Went – Auxin</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">2. Kurosawa – Cytokinin</label><br />
    <input name="question3" type="radio" />
    <label>3. H.H. Cousins- Ethylene</label><br />
    <input name="question3" type="radio" />
    <label>4. F. Skoog- Cytokinin</label></div><hr />

### 4. Germination in which of the following plants is epigeal?
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. Maize</label><br />
    <input name="question4" type="radio" />
    <label>2. Mango</label><br />
    <input name="question4" type="radio" />
    <label>3. Radish</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">4. Bean</label></div><hr />

### 5. Which is most conspicuous event in any living organism?
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Reproduction</label><br />
    <input name="question5" type="radio" />
    <label>2. Maturation</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">3. Growth</label><br />
    <input name="question5" type="radio" />
    <label>4. Differentiation</label></div><hr />

### 6. Maize apical meristem gives rise to:
  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. 3,50,000 cells per hour</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">2. 17,500 cells per hour</label><br />
    <input name="question6" type="radio" />
    <label>3. 3,500 cells per day</label><br />
    <input name="question6" type="radio" />
    <label>4. 17,500 cells per day</label></div><hr />

### 7. Cells in meristematic phase of growth have which of following characteristics?
A. Rich in protoplasm.  
B. Large nucleus.  
C. Abundant cytoplasmic connections.  
D. Thin cell wall.
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. Only A and C</label><br />
    <input name="question7" type="radio" />
    <label>2. A, C, D only</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">3. A,B,C,D</label><br />
    <input name="question7" type="radio" />
    <label>4. Only A and B</label></div><hr />

### 8. Choose the incorrect option:
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. Embryo development shows both arithmetic and geometric phases of growth.</label><br />
    <input name="question8" type="radio" />
    <label>2. Increased vacuolation is seen during phase of elongation.</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">3. In most systems, initial growth if fast and it decreases rapidly thereafter.</label><br />
    <input name="question8" type="radio" />
    <label>4. Relative growth refers to efficiency index.</label></div><hr />

### 9. During which process cells undergo few to major structural changes in their cell walls?
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Growth</label><br />
    <input name="question9" type="radio" />
    <label>2. Maturation</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">3. Differentiation</label><br />
    <input name="question9" type="radio" />
    <label>4. Dedifferentiation</label></div><hr />

### 10. Choose the incorrect option with respect to development of tracheary element.
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Loss of protoplasm</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">2. Inelastic cell wall</label><br />
    <input name="question10" type="radio" />
    <label>3. Lignin deposition</label><br />
    <input name="question10" type="radio" />
    <label>4. Cell wall becomes very strong</label></div><hr />

### 11. Odd one out with respect to dedifferentiation
  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. Interfascicular cambium</label><br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">2. Intrafascicular cambium</label><br />
    <input name="question11" type="radio" />
    <label>3. Cork cambium</label><br />
    <input name="question11" type="radio" />
    <label>4. Root vascular cambium</label></div><hr />

### 12. Extrinsic factor among the following which controls development.
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. Intercellular factor</label><br />
    <input name="question12" type="radio" />
    <label>2. Intracellular factor</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">3. Nutrition</label><br />
    <input name="question12" type="radio" />
    <label>4. Both 1 and 3</label></div><hr />

### 13. Choose the incorrect match:
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Auxin – Indole acetic acid</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">2. Zeatin – N⁶ fufurylamino purine</label><br />
    <input name="question13" type="radio" />
    <label>3. Abscissic acid – Carotenoids</label><br />
    <input name="question13" type="radio" />
    <label>4. Gibberellic acid – Terpene</label></div><hr />

### 14. Which PGR plays important role in response to wounds?
  <div className="tb-mcq">
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">1. ABA</label><br />
    <input name="question14" type="radio" />
    <label>2. GAs</label><br />
    <input name="question14" type="radio" />
    <label>3. IAA</label><br />
    <input name="question14" type="radio" />
    <label>4. Cytokinin</label></div><hr />

### 15. Choose the correct statement.
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Kinetin is obtained for coconut milk.</label><br />
    <input name="question15" type="radio" />
    <label>2. Auxin was first isolated from oat seedlings.</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">3. Ethylene is largely an inhibitor of growth activities.</label><br />
    <input name="question15" type="radio" />
    <label>4. Dormin was chemically different from ABA.</label></div><hr />

### 16. Growth regulator used to induce rooting in twig is:
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. Ethylene</label><br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">2. Auxin</label><br />
    <input name="question16" type="radio" />
    <label>3. Cytokinin</label><br />
    <input name="question16" type="radio" />
    <label>4. Gibberellic acids</label></div><hr />

### 17. H.H. cousins confirmed release of volatile substance from P that hastened ripening of stored Q. P and Q are:
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. Banana and apple</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">2. Orange and banana</label><br />
    <input name="question17" type="radio" />
    <label>3. Banana and orange</label><br />
    <input name="question17" type="radio" />
    <label>4. Apple and banana</label></div><hr />

### 18. ABA plays an important role in
  <div className="tb-mcq">
    <input name="question18" type="radio" />
    <label>1. Seed development</label><br />
    <input name="question18" type="radio" />
    <label>2. Seed maturation</label><br />
    <input name="question18" type="radio" />
    <label>3. Seed dormancy</label><br />
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">4. All of these</label></div><hr />

### 19. State True or False:
A. Any PGR has diverse physiological effects on plants.  
B. Diverse PGR manifest similar effects.
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. T,F</label><br />
    <input name="question19" type="radio" />
    <label>2. F,T</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">3. T,T</label><br />
    <input name="question19" type="radio" />
    <label>4. F,T</label></div><hr />

### 20. Odd one with respect to chemical inhibitors for seed dormancy.
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1. Ascorbic acid</label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">2. Nitrates</label><br />
    <input name="question20" type="radio" />
    <label>3. Phenolic acid</label><br />
    <input name="question20" type="radio" />
    <label>4. Abscissic acids</label></div>