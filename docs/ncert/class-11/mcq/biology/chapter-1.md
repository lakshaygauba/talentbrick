---
sidebar_position: 1
title: MCQ Questions The Living World Biology Chapter 1 Class 11
description: The Living World Biology MCQ Questions Class 11, These are the latest questions to expect in NEET | School Exams | Competitive Exams.
image: https://icdn.talentbrick.com/mcq/MCQ-Questions-Class-11-Biology-Chapter-1-The-Living-World-www.talentbrick.com_.png
sidebar_label: Chapter 1
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

**NCERT MCQ Questions** for Class 11 Biology Chapter 1 The Living World for NEET | School Exams | Class 11.

Prepare these important MCQ Questions of Class 11 Biology Chapter 1 The Living World with Answers, These are Latest questions to expect in **NEET | School Exams**.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. Systematics is a __ word.
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Greek</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">2. Latin</label><br />
    <input name="question1" type="radio" />
    <label>3. German</label><br />
    <input name="question1" type="radio" />
    <label>4. French</label><hr />
  </div>

### 2. Who is known as Darwin of 20ᵗʰ century.
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Norman Mayr</label><br />
    <input name="question2" type="radio" />
    <label>2. Thomas Malthus</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">3. Ernst Mayr</label><br />
    <input name="question2" type="radio" />
    <label>4. GN Ramachandran</label><hr />
  </div>

### 3. Choose the correct statement.
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. Photoperiod affects reproduction in plants only.</label><br />
    <input name="question3" type="radio" />
    <label>2. Most obvious featur of all living organisms is growth.</label><br />
    <input name="question3" type="radio" />
    <label>3. Technically complicated feature of all living organisms is metabolism.</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">4. Emergence is feature of living organism.</label><hr />
  </div>

### 4. Which of the following multiply by fragmentation?
  <div className="tb-mcq">
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">1. Protonema of mosses</label><br />
    <input name="question4" type="radio" />
    <label>2. Protonema of pteridophytes</label><br />
    <input name="question4" type="radio" />
    <label>3. Unicellular Algae</label><br />
    <input name="question4" type="radio" />
    <label>4. Amoeba</label><hr />
  </div>

### 5. For plants principles and criteria for scientific naming is given by
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. ICNB</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">2. ICBN</label><br />
    <input name="question5" type="radio" />
    <label>3. ICZN</label><br />
    <input name="question5" type="radio" />
    <label>4. Both 1 and 2</label><hr />
  </div>

### 6. Biological name has how many components?
  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>a. 6</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">b. 2</label><br />
    <input name="question6" type="radio" />
    <label>c. 3</label><br />
    <input name="question6" type="radio" />
    <label>d. 5</label><hr />
  </div>

### 7. Find the incorrect statement.
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. Zoo enables us to learn about food habits and behaviour of animals.</label><br />
    <input name="question7" type="radio" />
    <label>2. All living organisms are linked to one another by the sharing of the common genetic material, but to varying degrees.</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">3. Primates have the property of self-consciousness.</label><br />
    <input name="question7" type="radio" />
    <label>4. The herbarium sheet carry a label providing information about collector’s name.</label>
  </div><hr />

### 8. Index to plant species found in particular area is given by
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. Catalogues</label><br />
    <input name="question8" type="radio" />
    <label>2. Monographs</label><br />
    <input name="question8" type="radio" />
    <label>3. Manuals</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">4. Flora</label>
  </div><hr />

### 9. State True/False
a. All animals in a zoo are provided, as far as possible, the conditions similar to their natural habitats.  
b. The keys are based on the contrasting characters generally in a pair called couplet.
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Only 2nd is false</label><br />
    <input name="question9" type="radio" />
    <label>2. Both are false</label><br />
    <input name="question9" type="radio" />
    <label>3. Only 1st is false</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">4. Both are true</label>
  </div><hr />

### 10. Indian botanical garden situated in _____
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Delhi</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">2. Howrah</label><br />
    <input name="question10" type="radio" />
    <label>3. Lucknow</label><br />
    <input name="question10" type="radio" />
    <label>4. Kew</label>
  </div><hr />

### 11. Mango belong to which order
  <div className="tb-mcq">
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">1. Sapindales</label><br />
    <input name="question11" type="radio" />
    <label>2. Anacardiaceae</label><br />
    <input name="question11" type="radio" />
    <label>3. Poales</label><br />
    <input name="question11" type="radio" />
    <label>4. Polymoniales</label>
  </div><hr />

### 12. Solanaceae are included in the order Polymoniales mainly based on:
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. Vegetative features</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">2. Floral Characters</label><br />
    <input name="question12" type="radio" />
    <label>3. Root Structures</label><br />
    <input name="question12" type="radio" />
    <label>4. Both 1 and 2</label>
  </div><hr />

### 13. No. of species have the descriped range between
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. 1 – 1.5 Million</label><br />
    <input name="question13" type="radio" />
    <label>2. 2.7 – 2.8 Million</label><br />
    <input name="question13" type="radio" />
    <label>3. 1.7 – 2.7 Million</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">4. 1.7 – 1.8 Million</label>
  </div><hr />

### 14. Which of the following can be considered as Taxa?
  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>1. Dog</label><br />
    <input name="question14" type="radio" />
    <label>2. Wheat</label><br />
    <input name="question14" type="radio" />
    <label>3. Mammals</label><br />
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">4. All of these</label>
  </div><hr />

### 15. What is the common name of Panthera pardus?
  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>1. Cheetah</label><br />
    <input id="q15" name="question14" type="radio" />
    <label htmlFor="q15">2. Leopard</label><br />
    <input name="question14" type="radio" />
    <label>3. Wolf</label><br />
    <input name="question14" type="radio" />
    <label>4. Lion</label>
  </div>