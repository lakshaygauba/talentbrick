---
sidebar_position: 9
title: MCQ Questions Biomolecules Biology Class 11 Chapter 9
description: Biomolecules Chapter 9 Biology Class 11, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-11-bio-chapter-9.png
sidebar_label: Chapter 9
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

MCQ Quiz Biomolecules Chapter 9 Biology Class 11 MCQ Questions for NEET |  School Exams.

Prepare these important MCQ Questions of Biomolecules (Biology), These are Latest questions to expect in NEET | School Exams.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. To perform chemical analysis acid used is X and we obtain Y slurry. X and Y are:
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Trifluoroacetic acid, thin</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">2. Trichloroacetic acid, thick</label> <br />
    <input name="question1" type="radio" />
    <label>3. Trichloroacetic acid, thin</label><br />
    <input name="question1" type="radio" />
    <label>4. Trifluoroacetic acid, thick</label>
  </div><hr />

### 2. Analytical techniques give us an idea of:
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Molecular formula</label><br />
    <input name="question2" type="radio" />
    <label>2. Probable structure</label><br />
    <input name="question2" type="radio" />
    <label>3. Reactivity with water</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">4. Both 1 and 2</label></div><hr />

### 3. Choose the incorrect statement:
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. Silicon is the second most abundant on the earth crust</label><br />
    <input name="question3" type="radio" />
    <label>2. Destructive experiment is to be done to detect inorganic compounds</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">3. Most abundant organic compound is water</label><br />
    <input name="question3" type="radio" />
    <label>4. All the carbon compounds that we get from living beings can be called biomolecules</label></div><hr />

  ### 4. Analysis of compounds gives an idea of:
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. Carbon</label><br />
    <input name="question4" type="radio" />
    <label>2. Hydrogen</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">3. Organic constituents</label><br />
    <input name="question4" type="radio" />
    <label>4. Both 1 and 2</label>
  </div><hr />

### 5. Based on the nature of variable group R there are how many amino acids?
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. 20</label><br />
    <input name="question5" type="radio" />
    <label>2. 24</label><br />
    <input name="question5" type="radio" />
    <label>3. 19</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">4. Many</label></div><hr />

### 6. Palmitic acid has:
  <div className="tb-mcq">
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">1. 16 carbon including carboxyl carbon</label><br />
    <input name="question6" type="radio" />
    <label>2. 15 carbon including carboxyl carbon</label><br />
    <input name="question6" type="radio" />
    <label>3. 17 carbon including carboxyl carbon</label><br />
    <input name="question6" type="radio" />
    <label>4. 16 carbon excluding carboxyl carbon</label></div><hr />

### 7. Lipids are classified as fats and oils based on
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. Boiling point</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">2. Melting point</label><br />
    <input name="question7" type="radio" />
    <label>3. Saturation or unsaturation</label><br />
    <input name="question7" type="radio" />
    <label>4. Presence or absence of phosphorous</label></div><hr />

### 8. Which tissue especially has lipids with complex structures:
  <div className="tb-mcq">
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">1. Neural tissue</label><br />
    <input name="question8" type="radio" />
    <label>2. Cardiac tissue</label><br />
    <input name="question8" type="radio" />
    <label>3. Connective tissue</label><br />
    <input name="question8" type="radio" />
    <label>4. Epithelial tissue</label></div><hr />

### 9. There are how many types of nucleotides?
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>a. 4</label><br />
    <input name="question9" type="radio" />
    <label>b. 2</label><br />
    <input name="question9" type="radio" />
    <label>c. 1</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">d. 5</label>
  </div><hr />

### 10. Choose the correct statement:
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Adenine has only one oxygen atom in its structure</label><br />
    <input name="question10" type="radio" />
    <label>2. Cholesterol has 25 carbon in its structure</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">3. Nucleic acids like DNA and RNA consists of nucleotides only</label><br />
    <input name="question10" type="radio" />
    <label>4. All of the above</label>
  </div><hr />

### 11. The most exciting aspect of chemistry deals with:
  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. Knowing the molecular structure of various compounds</label><br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">2. Isolation of thousands of compound</label><br />
    <input name="question11" type="radio" />
    <label>3. Classifying compound as organic and inorganic</label><br />
    <input name="question11" type="radio" />
    <label>4. Synthesizing newer compounds</label>
  </div><hr />

### 12. Choose the incorrect statement:
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. All compounds found in the acid-soluble pool have their, molecular weights ranging from 18 to 800 Da.</label><br />
    <input name="question12" type="radio" />
    <label>2. Polysaccharides, nucleic acids, Proteins comprise the macromolecular fraction of any living tissue.</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">3. When we grind tissue we do not disturb the cell structure.</label><br />
    <input name="question12" type="radio" />
    <label>4. Percentage of protein is about 10 – 15 in cellular mass.</label></div><hr />

### 13. Polysaccharides are found in:
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Acid soluble pellet</label><br />
    <input name="question13" type="radio" />
    <label>2. Acid insoluble supernatant</label><br />
    <input name="question13" type="radio" />
    <label>3. Acid soluble supernatant</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">4. Acid insoluble pellet</label></div><hr />

### 14. Complex polysaccharides are mostly:
  <div className="tb-mcq">
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">1. Homopolymers</label><br />
    <input name="question14" type="radio" />
    <label>2. Heteropolymers</label><br />
    <input name="question14" type="radio" />
    <label>3. Copolymers</label><br />
    <input name="question14" type="radio" />
    <label>4. Branched-chain polymers</label></div><hr />

### 15. A nucleotide has how many chemically distinct components:
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>a. 2</label><br />
    <input name="question15" type="radio" />
    <label>b. 4</label><br />
    <input name="question15" type="radio" />
    <label>c. 1</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">d. 3</label>
  </div><hr />

### 16. In proteins:
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. Only left-handed helices are observed</label><br />
    <input name="question16" type="radio" />
    <label>2. Helices nature change with temperature</label><br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">3. Only right-handed helices are observed</label><br />
    <input name="question16" type="radio" />
    <label>4. Both left and right handed helices are observed</label></div><hr />

### 17. Nucleic acids exhibit ___ variety of secondary structures:
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. Little</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">2. Wide</label><br />
    <input name="question17" type="radio" />
    <label>3. No variety</label><br />
    <input name="question17" type="radio" />
    <label>4. Almost all</label></div><hr />

### 18. One of the greatest discovery ever made was:
  <div className="tb-mcq">
    <input name="question18" type="radio" />
    <label>1. 10 metabolic steps in Glycolysis</label><br />
    <input name="question18" type="radio" />
    <label>2. Principle of Bioenergetics</label><br />
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">3. All biomolecules have a turnover</label> <br />
    <input name="question18" type="radio" />
    <label>4. Structure of alpha-helix</label></div><hr />

### 19. Choose the correct statement:
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. Majority of metabolic reactions occur in isolation.</label>
    <input name="question19" type="radio" />
    <label>2. Glucose in a normal individual is 4.2mol/L – 6.1 mol/L.</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">3. Every chemical reaction is a catalyzed reaction.</label><br />
    <input name="question19" type="radio" />
    <label>4. Metabolic pathways are circular only.</label></div><hr />

### 20. Choose the correct statement:
A. Metabolic reactions are always linked to some other reactions.  
B. Flow of metabolite through metabolic pathway is at definite rate.
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1. Both A and B are incorrect</label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">2.Both A and B are correct</label><br />
    <input name="question20" type="radio" />
    <label>3. Only A is correct</label><br />
    <input name="question20" type="radio" />
    <label>4. Only B is correct</label></div><hr />

### 21. Cholesterol is derived from:
  <div className="tb-mcq">
    <input name="question21" type="radio" />
    <label>1. Butryric acid</label><br />
    <input name="question21" type="radio" />
    <label>2. Ciric acid</label><br />
    <input id="q21" name="question21" type="radio" />
    <label htmlFor="q21">3. Acetic acid</label><br />
    <input name="question21" type="radio" />
    <label>4. Amino acids</label></div><hr />

### 22. Metabolism is synonymous to:
  <div className="tb-mcq">
    <input name="question22" type="radio" />
    <label>1. Equilibrium state</label><br />
    <input id="q22" name="question22" type="radio" />
    <label htmlFor="q22">2. Living state</label><br />
    <input name="question22" type="radio" />
    <label>3.Non steady state</label> <br />
    <input name="question22" type="radio" />
    <label>4. Transition state</label></div><hr />

### 23. In absence of any enzyme rate of formation of carbonic acid is about
  <div className="tb-mcq">
    <input name="question23" type="radio" />
    <label>1. 200 molecules per second</label><br />
    <input name="question23" type="radio" />
    <label>2. 600000 molecules per hour</label><br />
    <input id="q23" name="question23" type="radio" />
    <label htmlFor="q23">3. 200 molecules per hour</label><br />
    <input name="question23" type="radio" />
    <label>4. 600000 molecules per second</label></div><hr />

### 24. Glycolysis is __ step reaction and catalysed by ___ different enzymes.
  <div className="tb-mcq">
    <input name="question24" type="radio" />
    <label>1. 10, 5</label><br />
    <input name="question24" type="radio" />
    <label>2. 12,10</label><br />
    <input name="question24" type="radio" />
    <label>3. 10,12</label><br />
    <input id="q24" name="question24" type="radio" />
    <label htmlFor="q24">4. 10,10</label></div><hr />

### 25. Intermediate formed in Enzyme action is:
  <div className="tb-mcq">
    <input name="question25" type="radio" />
    <label>1. ES complex</label><br />
    <input id="q25" name="question25" type="radio" />
    <label htmlFor="q25">2. EP complex</label><br />
    <input name="question25" type="radio" />
    <label>3. PS complex</label><br />
    <input name="question25" type="radio" />
    <label>4. Both 2 and 3</label></div><hr />

### 26. Succinate dehydrogenase is inhibited by:
  <div className="tb-mcq">
    <input id="q26" name="question26" type="radio" />
    <label htmlFor="q26">1. Malonate</label><br />
    <input name="question26" type="radio" />
    <label>2. Malate</label><br />
    <input name="question26" type="radio" />
    <label>3. Aspartate</label><br />
    <input name="question26" type="radio" />
    <label>4. Oxalosuccinate</label></div><hr />

### 27. In enzyme classification there are how many subclasses for each class?
  <div className="tb-mcq">
    <input name="question27" type="radio" />
    <label>a. 2-6</label><br />
    <input id="q27" name="question27" type="radio" />
    <label htmlFor="q27">b. 4-13</label><br />
    <input name="question27" type="radio" />
    <label>c. 6</label><br />
    <input name="question27" type="radio" />
    <label>d. 13-20</label></div><hr />

### 28. Enzymes are named according to how many no. of digits?
  <div className="tb-mcq">
    <input id="q28" name="question28" type="radio" />
    <label htmlFor="q28">a. 4 </label><br />
    <input name="question28" type="radio" />
    <label>b. 6</label><br />
    <input name="question28" type="radio" />
    <label>c. 2</label><br />
    <input name="question28" type="radio" />
    <label>d. 9</label><hr />
  </div>

### 29. Enzymes catalysing transfer of group H between a pair of substrate is:
  <div className="tb-mcq">
    <input name="question29" type="radio" />
    <label>1. Transferases</label><br />
    <input name="question29" type="radio" />
    <label>2. Isomerases</label><br />
    <input id="q29" name="question29" type="radio" />
    <label htmlFor="q29">3. Dehydrogenase</label><br />
    <input name="question29" type="radio" />
    <label>4. Lyases</label></div><hr />

### 30. Choose the incorrect statement:
  <div className="tb-mcq">
    <input name="question30" type="radio" />
    <label>1. Co-enzymes are organic compounds</label><br />
    <input name="question30" type="radio" />
    <label>2. Metal ions forms coordinate bonds with side chains</label><br />
    <input id="q30" name="question30" type="radio" />
    <label htmlFor="q30">3. Prosthetic group haem is part of site other than active site of enzyme</label><br />
    <input name="question30" type="radio" />
    <label>4. NAD has two phosphate group in its structure</label></div>