---
sidebar_position: 21
title: MCQ Questions Neural Control and Coordination Biology Class 11 Ch 21
description: Neural Control and Coordination Biology, MCQs for Class 11, These are the latest questions to expect in NEET | School Exams | Competitive Exams.
image: https://icdn.talentbrick.com/mcq/class-11-bio-chapter-21.png
sidebar_label: Chapter 21
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

NCERT Biology Class 11 MCQ Practice Questions Neural Control and Coordination Chapter 21 for NEET | Class 11

Prepare these important MCQ Questions of Neural control and coordination in Biology, Latest questions to expect in NEET | School Exams.

### 1. Which of the following is not performed by neuron?
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Detect stimuli</label><br />
    <input name="question1" type="radio" />
    <label>2. Transmit stimuli</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">3. Generate stimuli</label><br />
    <input name="question1" type="radio" />
    <label>4. Receive stimuli</label></div><hr />

### 2. Choose the incorrect statement.
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. The neural system coordinates and integrates functions as well as metabolism of all the organs.</label><br />
    <input name="question2" type="radio" />
    <label>2. Very important part of forebrain is called Hypothalamus</label><br />
    <input name="question2" type="radio" />
    <label>3. Neural system of all animals is composed of highly specialised cells.</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">4. Invertebrates have a more developed neural system.</label></div><hr />

### 3. Choose the incorrect match.
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. Insects – Brain</label><br />
    <input name="question3" type="radio" />
    <label>2. Human – Central nervous system</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">3. Porifera- Neuron</label><br />
    <input name="question3" type="radio" />
    <label>4. Hydra- Network of neurons</label></div><hr />

### 4. Nerve fibres of PNS are of __ types.
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>a. 3</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">b. 2</label><br />
    <input name="question4" type="radio" />
    <label>c. 4</label><br />
    <input name="question4" type="radio" />
    <label>d. 5</label></div><hr />

### 5. Autonomic neural system transmits impulses from CNS to:
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Involuntary organs</label><br />
    <input name="question5" type="radio" />
    <label>2. Smooth muscles</label><br />
    <input name="question5" type="radio" />
    <label>3. Skeletal muscles</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">4. Both 1 and 2</label></div><hr />

### 6. Neurons in hydra are of which type?
  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. Unipolar</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">2. Apolar</label><br />
    <input name="question6" type="radio" />
    <label>3. Bipolar</label><br />
    <input name="question6" type="radio" />
    <label>4. Mutipolar</label></div><hr />

### 7. Visceral nervous system comprises of
A. Plexuses  
B. Ganglia  
C. Nerves  
D. Fibres
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. Only B and C</label><br />
    <input name="question7" type="radio" />
    <label>2. A, B, C only</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">3. A,B,C,D</label><br />
    <input name="question7" type="radio" />
    <label>4. Only A and B</label></div><hr />

### 8. Choose the incorrect option:
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. Neuron is microscopic structure</label><br />
    <input name="question8" type="radio" />
    <label>2. Axon hillock is part of Cyton</label><br />
    <input name="question8" type="radio" />
    <label>3. Axon is a long fibre</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">4. Based on number of axon and dendrites neurons are divided into 5 types.</label></div><hr />

### 9. Which of the following neuron have one axon and one dendrite
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Bipolar neuron</label><br />
    <input name="question9" type="radio" />
    <label>2. Unipolar neuron</label><br />
    <input name="question9" type="radio" />
    <label>3. Pseudounipolar neuron</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">4. Both 1 and 3</label></div><hr />

### 10. Choose the correct statement with respect to myelinated nerve fibres.
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Enclosed by schwann cells.</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">2. Enveloped by schwann cells.</label><br />
    <input name="question10" type="radio" />
    <label>3. Schwann do not enclose nor envelop them.</label><br />
    <input name="question10" type="radio" />
    <label>4. Commonly found in autonomous neural system.</label></div><hr />

### 11. Membrane is impermeable to __ present in axoplasm.
  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. Na⁺</label><br />
    <input name="question11" type="radio" />
    <label>2. Lipids</label><br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">3. Negatively charges proteins</label><br />
    <input name="question11" type="radio" />
    <label>4. Both 1 and 3</label></div><hr />

### 12. Ionic gradients across membrane are restored by:
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. ATP pump</label><br />
    <input name="question12" type="radio" />
    <label>2. Voltage gated Na⁺ channels</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">3. Voltage gated K⁺ channels</label><br />
    <input name="question12" type="radio" />
    <label>4. Leaky channels</label></div><hr />

### 13. Incorrect with respect to electrical synapse:
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Impulse flow is bidirectional.</label><br />
    <input name="question13" type="radio" />
    <label>2. Impulse transmission across an electrical synapse is always faster than chemical synapse.</label><br />
    <input name="question13" type="radio" />
    <label>3. Electrical synapse are rare in humans.</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">4. Transmission across them is similar to conduction along two axons.</label></div><hr />

### 14. Which part forms major part of human brain.
  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>1. Thalamus</label><br />
    <input name="question14" type="radio" />
    <label>2. Hypothalamus</label><br />
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">3. Cerebrum</label><br />
    <input name="question14" type="radio" />
    <label>4. Medulla</label></div><hr />

### 15. Cerebral cortex contains:
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Small regions , neither sensory nor motor in function.</label><br />
    <input name="question15" type="radio" />
    <label>2. Large regions , either sensory nor motor in function.</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">3. Large regions , neither sensory nor motor in function.</label><br />
    <input name="question15" type="radio" />
    <label>4. Small regions , both sensory and motor in function.</label></div><hr />

### 16. Which of the following is not the function of association area?
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. Intersensory association</label><br />
    <input name="question16" type="radio" />
    <label>2. Memory</label><br />
    <input name="question16" type="radio" />
    <label>3. Communication</label><br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">4. Expression of emotions</label></div><hr />

### 17. Major coordinating centre for motor and sensory signalling is:
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. Hypothalamus</label><br />
    <input name="question17" type="radio" />
    <label>2. Cerebellum</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">3. Thalamus</label><br />
    <input name="question17" type="radio" />
    <label>4. Cerebrum</label></div><hr />

### 18. Choose the correct statement:
  <div className="tb-mcq">
    <input name="question18" type="radio" />
    <label>1. Excitor transmits impulse to CNS.</label><br />
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">2. Motor end plate receives impulse from CNS.</label><br />
    <input name="question18" type="radio" />
    <label>3. Pons connect brain and spinal cord</label><br />
    <input name="question18" type="radio" />
    <label>4. Interneuron is present in white matter of spinal cord.</label></div><hr />

### 19. State True or False:
A. Sensory organs detect all types of changes in environment.  
B. Olfactory receptors are made of epithelium.
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. T,F</label><br />
    <input name="question19" type="radio" />
    <label>2. F,T</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">3. T,T</label><br />
    <input name="question19" type="radio" />
    <label>4. F,T</label></div><hr />

### 20. Which is extension of brain’s limbic system.
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1. Olfactory epithelium</label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">2. Olfactory bulb</label><br />
    <input name="question20" type="radio" />
    <label>3. Olfactory bipolar cells</label><br />
    <input name="question20" type="radio" />
    <label>4. Bowmann’s gland</label></div><hr />

### 21. External layer of eye is composed of:
  <div className="tb-mcq">
    <input name="question21" type="radio" />
    <label>1. Loose connective tissue</label><br />
    <input name="question21" type="radio" />
    <label>2. Specialised connective tissue</label><br />
    <input id="q21" name="question21" type="radio" />
    <label htmlFor="q21">3. Dense connective tissue</label><br />
    <input name="question21" type="radio" />
    <label>4. Epithelial tissue</label></div><hr />

### 22. Lens is held in place by:
  <div className="tb-mcq">
    <input name="question22" type="radio" />
    <label>1. Ciliary muscles</label><br />
    <input id="q22" name="question22" type="radio" />
    <label htmlFor="q22">2. Ligaments</label><br />
    <input name="question22" type="radio" />
    <label>3. Iris</label><br />
    <input name="question22" type="radio" />
    <label>4. Cornea</label></div><hr />

### 23. Choose the correct with respect to location.
  <div className="tb-mcq">
    <input name="question23" type="radio" />
    <label>1. Blind spot is medial to and slightly below posterior pole of eye ball.</label><br />
    <input name="question23" type="radio" />
    <label>2. Lateral to blind spot there is pigmented spot called Fovea.</label><br />
    <input name="question23" type="radio" />
    <label>3. Blind spot is lateral to and slightly below posterior pole of eye ball.</label><br />
    <input id="q23" name="question23" type="radio" />
    <label htmlFor="q23">4. Lateral to blind spot there is pigmented spot called Macula lutea.</label></div><hr />

### 24. Tympanic membrane is part of:
  <div className="tb-mcq">
    <input id="q24" name="question24" type="radio" />
    <label htmlFor="q24">1. External ear</label><br />
    <input name="question24" type="radio" />
    <label>2. Middle ear</label><br />
    <input name="question24" type="radio" />
    <label>3. Cochlea</label><br />
    <input name="question24" type="radio" />
    <label>4. Vestibular apparatus</label></div><hr />

### 25. Auditory sense is perceived in which lobe of brain?
  <div className="tb-mcq">
    <input name="question25" type="radio" />
    <label>1. Parietal</label><br />
    <input id="q25" name="question25" type="radio" />
    <label htmlFor="q25">2. Temporal</label><br />
    <input name="question25" type="radio" />
    <label>3. Frontal</label><br />
    <input name="question25" type="radio" />
    <label>4. Occipital</label></div><hr />

### 26. Tympanic membrane consists of:
  <div className="tb-mcq">
    <input name="question26" type="radio" />
    <label>1. Connective tissue covered with skin on both the sides.</label><br />
    <input name="question26" type="radio" />
    <label>2. Connective tissue covered with skin on inner side and mucus membrane on outer side.</label><br />
    <input id="q26" name="question26" type="radio" />
    <label htmlFor="q26">3. Connective tissue covered with skin on outer side and mucus membrane on inner side.</label><br />
    <input name="question26" type="radio" />
    <label>4. Mucus membrane on both the sides.</label></div><hr />

### 27. Scala tympani terminates at the X which opens to Y. X and Y are respectively:
  <div className="tb-mcq">
    <input id="q27" name="question27" type="radio" />
    <label htmlFor="q27">1. Round window , Middle ear</label><br />
    <input name="question27" type="radio" />
    <label>2. Oval window , Middle ear</label><br />
    <input name="question27" type="radio" />
    <label>3. Round window , Inner ear</label><br />
    <input name="question27" type="radio" />
    <label>4. Oval window , Inner ear</label></div><hr />

### 28. Above the rows of hair cells there is:
  <div className="tb-mcq">
    <input name="question28" type="radio" />
    <label>1. Thick elastic membrane , Tectorial membrane.</label><br />
    <input id="q28" name="question28" type="radio" />
    <label htmlFor="q28">2. Thin elastic membrane , Tectorial membrane.</label><br />
    <input name="question28" type="radio" />
    <label>3. Thick elastic membrane , Basilar membrane.</label><br />
    <input name="question28" type="radio" />
    <label>4. Thin elastic membrane , Reissner’s membrane.</label></div><hr />

### 29. State true or false.
A. Nerve impulses are generated and transmitted by afferent fibres to auditory cortex of brain.  
B. Vestibular apparatus is located below Cochlea.
  <div className="tb-mcq">
    <input id="q29" name="question29" type="radio" />
    <label htmlFor="q29">1. T,F</label><br />
    <input name="question29" type="radio" />
    <label>2. T,T</label><br />
    <input name="question29" type="radio" />
    <label>3. F,T</label><br />
    <input name="question29" type="radio" />
    <label>4. F,F</label></div><hr />

### 30. Which of the following integrates information received from semicircular canals and the auditory system.
  <div className="tb-mcq">
    <input name="question30" type="radio" />
    <label>1. Temporal lobe</label><br />
    <input id="q30" name="question30" type="radio" />
    <label htmlFor="q30">2. Cerebellum</label><br />
    <input name="question30" type="radio" />
    <label>3. Medulla</label><br />
    <input name="question30" type="radio" />
    <label>4. Thalamus</label></div>