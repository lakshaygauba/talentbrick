---
sidebar_position: 2
title: MCQ Questions Biological Classification Class 11 Chapter 2
description: Biological Classification MCQ Questions Class 11 Biology, These are the latest questions to expect in NEET | School Exams | Competitive Exams.
image: https://icdn.talentbrick.com/mcq/class-11-bio-chapter-2.png
sidebar_label: Chapter 2
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

**NCERT MCQ Quiz** for Class 11 Biology Chapter 2 Biological Classification for NEET | School Exams | Class 11.

Prepare these important MCQ Questions of Class 11 Biology Chapter 2 Biological Classification with Answers, These are Latest questions to expect in **NEET | School Exams**.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. Biological classification of plants and animals was first proposed by:
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Linnaeus</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">2. Aristotle</label><br />
    <input name="question1" type="radio" />
    <label>3. R.H. Whittaker</label><br />
    <input name="question1" type="radio" />
    <label>4. Theophrastus</label></div><hr />

### 2. Choose the correct statement.
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Aristotle used simple morphological characters to classify animals&nbsp;</label><br />
    <input name="question2" type="radio" />
    <label>2. Monera have cellulosic cell wall</label><br />
    <input name="question2" type="radio" />
    <label>3. Loose tissue body organization is seen in kingdom Plantae</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">4. In two kingdom classification, kingdoms included all plants and animals</label></div><hr />

### 3. How many main criteria were used by RH whittaker?
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>a. 3</label><br />
    <input name="question3" type="radio" />
    <label>b. 4</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">c. 5</label><br />
    <input name="question3" type="radio" />
    <label>d. 6</label><hr />
  </div>

### 4. Comma shaped bacteria are called:
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. Bacillus</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">2. Vibrio</label><br />
    <input name="question4" type="radio" />
    <label>3. Cocci</label><br />
    <input name="question4" type="radio" />
    <label>4. Both 1 and 2</label></div><hr />

### 5. Majority of bacteria are:
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Photosynthetic</label><br />
    <input name="question5" type="radio" />
    <label>2. Chemosynthetic</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">3. Decomposers</label> <br />
    <input name="question5" type="radio" />
    <label>4. Parasitic</label></div><hr />

### 6. Which of the following have pigments similar to green plants?
  <div className="tb-mcq">
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">1. Cyanobacteria</label><br />
    <input name="question6" type="radio" />
    <label>2. Euglenoids&nbsp;</label><br />
    <input name="question6" type="radio" />
    <label>3. Fungi</label><br />
    <input name="question6" type="radio" />
    <label>4. Archaebacteria</label></div><hr />

### 7. Which of the following are oxidised by chemosynthetic bacteria ?
A. Nitrates  
B. Nitrites  
C. Ammonia
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. Only B and C&nbsp;</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">2. A, B, C</label><br />
    <input name="question7" type="radio" />
    <label>3. Only C</label><br />
    <input name="question7" type="radio" />
    <label>4. Only B</label></div><hr />

 ### 8. Choose the incorrect option.
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. Amoboid protozoans are found in moist soil.</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">2. In diatoms cell walls form two thick overlapping shell which fit together.</label><br />
    <input name="question8" type="radio" />
    <label>3. Dinoflagellates are mostly marine.</label><br />
    <input name="question8" type="radio" />
    <label>4. Dinoflagellates appear yellow, brown and blue.</label></div><hr />

### 9. Pigments of which organisms are identical to those of higher plants?
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Diatoms</label><br />
    <input name="question9" type="radio" />
    <label>2. Slime moulds</label><br />
    <input name="question9" type="radio" />
    <label>3. Cyanobacteria</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">4. Euglenoids</label></div><hr />

### 10. Spores of slime moulds are dispersed by:
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Water</label><br />
    <input name="question10" type="radio" />
    <label>2. Soil</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">3. Wind</label><br />
    <input name="question10" type="radio" />
    <label>4. Insects</label></div><hr />

### 11. Silica shells are found in:
  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. Freshwater amoeba</label><br />
    <input name="question11" type="radio" />
    <label>2. Sporozoans</label><br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">3. Marine amoeba</label><br />
    <input name="question11" type="radio" />
    <label>4. Ciliates</label></div><hr />

### 12. Most notorious organism is:
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. Mycoplasma</label><br />
    <input name="question12" type="radio" />
    <label>2. Cyanobacteria</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">3. Plasmodium</label><br />
    <input name="question12" type="radio" />
    <label>4. Mushroom</label></div><hr />

### 13. Which of the following statement is correct?
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. All fungi are filamentous.</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">2. Fusion of protoplasm is called plamogamy</label><br />
    <input name="question13" type="radio" />
    <label>3. The network of mycelium is called hyphae</label><br />
    <input name="question13" type="radio" />
    <label>4. Dikaryotic stage has ploidy (2n)</label></div><hr />

### 14. Mushroom and toadstools both have which character in common?
  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>1. Edible fungi</label><br />
    <input name="question14" type="radio" />
    <label>2. Ascomycetes</label><br />
    <input name="question14" type="radio" />
    <label>3. Parasitic fungi</label><br />
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">4. Reproduce by fragmentation</label></div><hr />

### 15. Ascomycetes are:
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Coprophilous</label><br />
    <input name="question15" type="radio" />
    <label>2. Decomposers</label><br />
    <input name="question15" type="radio" />
    <label>3. Parasitic</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">4. All of these</label></div><hr />

### 16. Identify the below given fungi
![Identify the fungi](https://icdn.talentbrick.com/mcq/Question-16.jpg)
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. Mucor</label><br />
    <input name="question16" type="radio" />
    <label>2. Rhizopus</label><br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">3. Aspergillus</label><br />
    <input name="question16" type="radio" />
    <label>4. Agaricus</label></div><hr />

### 17. In basidiomycetes karyogamy occurs in
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. Basidiocarp</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">2. Basidium</label><br />
    <input name="question17" type="radio" />
    <label>3. Basidiospore</label><br />
    <input name="question17" type="radio" />
    <label>4. Both 2 and 3</label></div><hr />

### 18. Choose the Correct for deuteromycetes:
  <div className="tb-mcq">
    <input name="question18" type="radio" />
    <label>1. Some members are decomposers or parasite while most are saprophytes</label><br />
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">2. Some members are parasitic or saprophytes while most are decomposers</label><br />
    <input name="question18" type="radio" />
    <label>3. Some members are parasites while most of them are decomposers or saprophytes</label><br />
    <input name="question18" type="radio" />
    <label>4. Some members are saprophytes while most are parasitic and decomposers</label></div><hr />

### 19. State true or false:
A. Plantae includes all eukaryotic chlorophyll containing organisms.  
B. The viruses are non cellular organisms.
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. T, F</label><br />
    <input name="question19" type="radio" />
    <label>2. F, T</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">3. T, T</label><br />
    <input name="question19" type="radio" />
    <label>4. F, T</label></div><hr />

### 20. The name virus was given by:
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1. Ivnowsky</label><br />
    <input name="question20" type="radio" />
    <label>2. Pasteur</label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">3. M.W Biejerinek</label><br />
    <input name="question20" type="radio" />
    <label>4. T.O. Diener</label></div>