---
sidebar_position: 6
title: MCQ Questions Molecular Basis of Inheritance Class 12 Biology Chapter 6 with Answers
description: Molecular Basis of Inheritance Chapter 6 Biology Class 12, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-12-bio-chapter-6.png
sidebar_label: Chapter 6
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

NCERT Biology Class 12th Bio Ch.6 Molecular Basis of Inheritance MCQ Practice Questions for NEET | Class 11

Prepare these important [MCQ Questions of Biology Class 12](/ncert/class-12/mcq/biology) Molecular Basis of Inheritance Chapter 6, Latest questions to expected to come in NEET | School Exams.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. For terminating process of translation release factor binds to

  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Ribosome subunit</label>
    <br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">2. Stop codon</label>
    <br />
    <input name="question1" type="radio" />
    <label>3. UTR at downstream</label>
    <br />
    <input name="question1" type="radio" />
    <label>4. tRNA</label>
  </div><hr />

### 2. Choose the incorrect statement.

  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Alec Jeffrey used satellite DNA as probe.</label><br />
    <input name="question2" type="radio" />
    <label>2. β gal stands for beta-galactosidase.</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">3. DNA from single cell is not enough to perform fingerprinting analysis.</label><br />
    <input name="question2" type="radio" />
    <label>4. Satellite DNA could be classified on basis of different base composition.</label></div><hr />

### 3. Choose the incorrect match with respect to codon and amino acid coded by them.

  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. UUC – Phe</label>
    <br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">2. AAU – Asp</label><br />
    <input name="question3" type="radio" />
    <label>3. GAG – Glu</label><br />
    <input name="question3" type="radio" />
    <label>4. UGU – Cys</label></div><hr />

### 4. How different proteins does the ribosome consists of?

  <div className="tb-mcq">
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">1. 80</label><br />
    <input name="question4" type="radio" />
    <label>2. 40</label><br />
    <input name="question4" type="radio" />
    <label>3. 25</label><br />
    <input name="question4" type="radio" />
    <label>4. 400</label></div><hr />

### 5. Process of translation begins when

  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Charging of tRNA takes place in presence of ATP.</label><br />
    <input name="question5" type="radio" />
    <label>2. Large subunit of ribosome encounters mRNA</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">3. Small subunit of ribosome encounters mRNA</label><br />
    <input name="question5" type="radio" />
    <label>4. tRNA binds UAC codon.</label></div><hr />

### 6. Amino acid found attached to tRNA having anticodon UCA is

  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. Tyrosine at 5′ end.</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">2. Serine at 3′ end.</label><br />
    <input name="question6" type="radio" />
    <label>3. Serine at 5′ end.</label><br />
    <input name="question6" type="radio" />
    <label>4. Tyrosine at 3′ end.</label></div><hr />

### 7. The actual structure of tRNA looks like:

  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. Clover leaf</label><br />
    <input name="question7" type="radio" />
    <label>2. Bean shaped</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">3. Inverted L</label><br />
    <input name="question7" type="radio" />
    <label>4. Double helix</label></div><hr />

### 8. Choose the incorrect option:

  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. Lac operon is under both positive and negative regulation.</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">2. Control of rate of translation is predominant site for control of gene expression in prokaryotes.</label><br />
    <input name="question8" type="radio" />
    <label>3. In most cases the sequences of operator bind a repressor protein.</label><br />
    <input name="question8" type="radio" />
    <label>4. In presence of lactose or allolactose, repressor is inactivated.</label></div><hr />

### 9. There are **i** sites in **ii** for amino acids to bind and be close enough for bond formation. i and ii are

  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. 3, large subunit</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">2. 2, large subunit</label><br />
    <input name="question9" type="radio" />
    <label>3. 2, small subunit</label><br />
    <input name="question9" type="radio" />
    <label>4. 3, small subunit</label></div><hr />

### 10. All the following amino acids are coded by six different codons except

  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Arginine</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">2. Lysine</label><br />
    <input name="question10" type="radio" />
    <label>3. Leucine</label><br />
    <input name="question10" type="radio" />
    <label>4. Serine</label></div><hr />

### 11. Chemical method developed by which scientist was instrumental in synthesis of RNA molecules with defined combination of bases?

  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. Jacob Monod</label><br />
    <input name="question11" type="radio" />
    <label>2. M.Nirenberg</label><br />
    <input name="question11" type="radio" />
    <label>3. G.Gamow</label><br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">4. HG Khorana</label></div><hr />

### 12. We face difficulty in predicting the codon sequence from the given amino acid sequence because of which property of genetic code

  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. Universal</label><br />
    <input name="question12" type="radio" />
    <label>2. Non ambiguous</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">3. Degeneracy</label><br />
    <input name="question12" type="radio" />
    <label>4. Comaless natture</label></div><hr />

### 13. Whic of the following,finally the helped the genetic code code to deciphered?

  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Severa ochoa</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">2. Cell free system for protein sythesis</label><br />
    <input name="question13" type="radio" />
    <label>3. Chemical method to for synthesis of RNA with defined combination of bases.</label><br />
    <input name="question13" type="radio" />
    <label>4. Lac operon elucidation</label></div><hr />

### 14. Vectors used in human genome project include

  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>1. BAC</label><br />
    <input name="question14" type="radio" />
    <label>2. YAC</label><br />
    <input name="question14" type="radio" />
    <label>3. Ti plasmid</label><br />
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">4. Both 1 and 2</label></div><hr />

### 15. Choose the incorrect statement.

  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Y chromosome has 231 genes.</label><br />
    <input name="question15" type="radio" />
    <label>2. The two alleles of a chromosome contain different copy numbers of VNTR. </label><br />
    <input name="question15" type="radio" />
    <label>3. DNA from every tissue from and individual show same degree of polymorphism.</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">4. Satellite DNA on density gradient centrifugation forms major peak.</label></div><hr />

### 16. QB bacteriophage has genetic material as

  <div className="tb-mcq">
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">1. RNA</label><br />
    <input name="question16" type="radio" />
    <label>2. ss DNA</label><br />
    <input name="question16" type="radio" />
    <label>3. ds DNA</label><br />
    <input name="question16" type="radio" />
    <label>4. Both RNA and DNA</label></div><hr />

### 17. ΦX174 phage has<

  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. 5386 base pairs</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">2. 5386 nucleotides</label><br />
    <input name="question17" type="radio" />
    <label>3. 48502 bp</label><br />
    <input name="question17" type="radio" />
    <label>4. about 4 billion bp</label></div><hr />

### 18. Unequivocal proof of DNA comes from study on

  <div className="tb-mcq">
    <input name="question18" type="radio" />
    <label>1. Bacteria</label><br />
    <input name="question18" type="radio" />
    <label>2. Neurospora</label><br />
    <input name="question18" type="radio" />
    <label>3. Humans</label><br />
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">4. Bacterial virus</label></div><hr />

### 19. State True or False:

A. The chains in B-DNA have parallel polarity.  
B. RNA has ability to duplicate direct their duplication.

  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. T, F</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">2. F, T</label><br />
    <input name="question19" type="radio" />
    <label>3. T, T</label><br />
    <input name="question19" type="radio" />
    <label>4. F, F</label></div><hr />

### 20. Blending is performed in Hershey chase experiment to

  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1.Seperate virus particles from bacteria </label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">2. Remove viral coats from bacteria</label><br />
    <input name="question20" type="radio" />
    <label>3. Infect the bactria with virus</label><br />
    <input name="question20" type="radio" />
    <label>4. Making DNA and protein radioactive</label></div>
