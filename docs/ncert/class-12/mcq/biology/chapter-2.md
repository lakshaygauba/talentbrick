---
sidebar_position: 2
title: MCQ Questions Sexual Reproduction in Flowering Plants Biology Chapter 2 Class 12
description: Sexual Reproduction in Flowering Plants Chapter 2 Biology Class 12, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-12-bio-chapter-2.png
sidebar_label: Chapter 2
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

MCQ Quiz Reproduction in Flowering Plants Class – 12th MCQ Questions for NEET |  School Exams | Class 12 Reproduction in Plants, Practice Questions.Prepare these important Questions of Reproduction in Plants, These are Latest questions to expect in NEET | School Exams.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. Find the incorrect statement:
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Seeds of large no. of species live for several years.</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">2. Oldest seed is of date palm 10,000 years of dormancy.</label><br />
    <input name="question1" type="radio" />
    <label>3. Fleshy fruit include guava.</label> <br />
    <input name="question1" type="radio" />
    <label>4. As seeds matures, water content is reduced by 10-15%</label>
  </div><hr />

### 2. Majority of plants produce:
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Parthenocarpic Fruit</label><br />
    <input name="question2" type="radio" />
    <label>2. Unisexual Flowers</label><br />
    <input name="question2" type="radio" />
    <label>3. False Fruit</label> <br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">4. Bisexual Flowers</label>
  </div><hr />

### 3. State True or False:
a. Only Sexual mode of reproduction is present in most of animals.  
b. Plant, fungi, animals differ greatly in External Morphology, internal structure, physiology but have similar pattern of sexual reproduction.  
c. In animals, juvenile phase is followed by change in physiology only.
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. FTF</label><br />
    <input name="question3" type="radio" />
    <label>2. TTT</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">3. TTF</label> <br />
    <input name="question3" type="radio" />
    <label>4. FFT</label>
  </div><hr />

### 4. Most crucial and Most vital event of sexual reproduction are:
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. Fertilisation</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">2. Syngamy</label><br />
    <input name="question4" type="radio" />
    <label>3. Gamete Transfer</label> <br />
    <input name="question4" type="radio" />
    <label>4. Copulation</label>
  </div><hr />

### 5. How many of following are Common pollinating agent?
  <div className="tb-mcq">
<table><tbody><tr><td>Bees, Gecko, Rodents, Sun Birds, Moths, ants, flies, wasps.</td></tr></tbody></table>
    <input name="question5" type="radio" />
    <label>a. 5</label><br />
    <input name="question5" type="radio" />
    <label>b. 8</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">c. 6</label> <br />
    <input name="question5" type="radio" />
    <label>d. 7</label>
  </div><hr />

### 6. Plants use __ biotic and ___ abiotic agents.
  <div className="tb-mcq">
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">a. 1,2</label><br />
    <input name="question6" type="radio" />
    <label>b. 2,1</label><br />
    <input name="question6" type="radio" />
    <label>c. 1,1</label> <br />
    <input name="question6" type="radio" />
    <label>d. 2,2</label>
  </div><hr />

### 7. Choose the Odd one out with respect to reward for pollination:
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. Safe Place</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">2. Fragrance</label><br />
    <input name="question7" type="radio" />
    <label>3. Nectar</label> <br />
    <input name="question7" type="radio" />
    <label>4. Pollen Grain</label>
  </div><hr />

### 8. Algae and Fungi shift to sexual method of reproduction ___ the onset of adverse condition.
  <div className="tb-mcq">
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">1. Just before</label><br />
    <input name="question8" type="radio" />
    <label>2. Just after</label><br />
    <input name="question8" type="radio" />
    <label>3. After reproducing asexual once.</label> <br />
    <input name="question8" type="radio" />
    <label>4. May be (a) &amp; (b)</label>
  </div><hr />

### 9. Flowers are object of:
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Ornamental</label><br />
    <input name="question9" type="radio" />
    <label>2. Religious</label><br />
    <input name="question9" type="radio" />
    <label>3. Social</label> <br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">4. All of these</label>
  </div><hr />

### 10. In how many among following male gamete is Transported through water?
  <div className="tb-mcq">
<table><tbody><tr><td>Vallisneria, Zostera, Maize, Marchantia, Chara, Pinus, Hydrilla.</td></tr></tbody></table>
    <input name="question10" type="radio" />
    <label>a. 5</label><br />
    <input name="question10" type="radio" />
    <label>b. 3</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">c. 2</label> <br />
    <input name="question10" type="radio" />
    <label>d. 4</label>
  </div><hr />

### 11. Radius of Pollen grain is about:
  <div className="tb-mcq">
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">a. 12.5-25u</label><br />
    <input name="question11" type="radio" />
    <label>b. 25-50u</label><br />
    <input name="question11" type="radio" />
    <label>c. 70u</label> <br />
    <input name="question11" type="radio" />
    <label>d. 100u</label>
  </div><hr />

### 12. Identify the Incorrect statement:
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. Vegetative reproduction is type of asexual reproduction.</label><br />
    <input name="question12" type="radio" />
    <label>2. Fungi and Algae reproduce through special asexual reproductive structures.</label><br />
    <input name="question12" type="radio" />
    <label>3. In yeast, the division is unequal</label> <br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">4. Water Hyacinth was introduced in India because of its flowers &amp; fruits.</label>
  </div><hr />

### 13. Which of the following statements are correct?
I : Much before actual flower is seen, the decision that plant is going to flower has taken place.  
II : Special cellular thickening in synergids help guiding pollen tube.
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>a. I only</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">b. I &amp; II Both</label><br />
    <input name="question13" type="radio" />
    <label>c. II only</label> <br />
    <input name="question13" type="radio" />
    <label>d. None</label>
  </div><hr />

### 14. State True or False (T/F):
- End products of sexual reproduction are fruits and seeds.
- In Angiosperms, there’s indirect pollination.
- Pollination in Yuca is brought by moth larvae.


  <div className="tb-mcq">
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">1. TTF</label><br />
    <input name="question14" type="radio" />
    <label>2. FFF</label><br />
    <input name="question14" type="radio" />
    <label>3. FTT</label> <br />
    <input name="question14" type="radio" />
    <label>4. TTT</label>
  </div><hr />

### 15. Tassels in corn cob are:
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Style</label><br />
    <input name="question15" type="radio" />
    <label>2. Stigma</label><br />
    <input name="question15" type="radio" />
    <label>3. Calyx</label> <br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">4. Both a &amp; b</label>
  </div>