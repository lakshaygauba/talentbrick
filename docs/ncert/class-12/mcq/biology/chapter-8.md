---
sidebar_position: 8
title: MCQ Questions Human Health and Disease Biology Class 12 Chapter 8
description: Human Health and Disease Chapter 8 Biology Class 12, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-12-bio-chapter-8.png
sidebar_label: Chapter 8
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

MCQ Questions Human Health and Disease Biology Class 12 Chapter 8 for NEET |  School Exams | Class 12 Reproduction in Plants, MCQ Practice Questions.

Prepare these important MCQ Questions of Biology Class 12 Human Health and Disease Ch.8, These are Latest questions to expect in NEET | School Exams.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. Non-infectious disease, major cause of death is
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Diabetes</label><br />
    <input name="question1" type="radio" />
    <label>2. HIV</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">3. Cancer</label><br />
    <input name="question1" type="radio" />
    <label>4. Anemia</label></div><hr />

### 2. Choose the correct statement.
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. All parasites are pathogen.</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">2. Constipation is one of the symptoms of amoebic dysentery.</label><br />
    <input name="question2" type="radio" />
    <label>3. In severe cases in common cold lips may turn blue or gray.</label><br />
    <input name="question2" type="radio" />
    <label>4. Common cold lasts for 3-7 weeks.</label></div><hr />

### 3. Malarial parasite reproduces __ in RBCs and __ in Liver.
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. Sexually, Sexually</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">2. Asexually, Asexually</label><br />
    <input name="question3" type="radio" />
    <label>3. Sexually, Asexually</label><br />
    <input name="question3" type="radio" />
    <label>4. Asexually, Sexually</label></div><hr />

### 4. Choose the correct statement with respect to stage that develops in RBCs:
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. Asexual stage, Gametocytes</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">2. Sexual stage, Gametocytes</label><br />
    <input name="question4" type="radio" />
    <label>3. Sexual stage, Sporozoites</label><br />
    <input name="question4" type="radio" />
    <label>4. Asexual stage, Sporozoites</label></div><hr />

### 5. Stools with excess mucous and blood clots is symptoms of:
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Cholera</label><br />
    <input name="question5" type="radio" />
    <label>2. Diphtheria</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">3. Amoebiasis</label><br />
    <input name="question5" type="radio" />
    <label>4. Dysentery</label></div><hr />

### 6. Internal bleeding and blockage of intestinal passage are symptoms of:
  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. Typhoid</label><br />
    <input name="question6" type="radio" />
    <label>2. Tuberculosis</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">3. Ascariasis</label><br />
    <input name="question6" type="radio" />
    <label>4. Amoebiasis</label></div><hr />

### 7. Which of the following are responsible for causing ringworms?
A. Trichoderma  
B. Trichophyton  
C. Microsporum  
D. Pin worm
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. B, C, D</label><br />
    <input name="question7" type="radio" />
    <label>2. A, B, C</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">3. Only B and C</label><br />
    <input name="question7" type="radio" />
    <label>4. Only A and B</label></div><hr />

### 8. Most common infectious diseases are:
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. AIDS and Common cold</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">2. Ringworm and Common cold</label><br />
    <input name="question8" type="radio" />
    <label>3. Cancer and AIDS</label><br />
    <input name="question8" type="radio" />
    <label>4. Common cold and Malaria</label></div><hr />

### 9. Elephantiasis results in gross deformities when it affects:
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Heart</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">2. Gonads</label><br />
    <input name="question9" type="radio" />
    <label>3. Lower limbs</label><br />
    <input name="question9" type="radio" />
    <label>4. Blood vessels</label></div><hr />

### 10. Acquired immunity is found in:
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Insects, vertebrates</label><br />
    <input name="question10" type="radio" />
    <label>2. Insects only</label><br />
    <input name="question10" type="radio" />
    <label>3. Both plants and animals</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">4. Vertebrates only</label></div><hr />

### 11. Choose odd one out with respect to phagocytic cells:
  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. Neutrophils</label><br />
    <input name="question11" type="radio" />
    <label>2. Macrophages</label><br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">3. Natural killer cells</label><br />
    <input name="question11" type="radio" />
    <label>4. Dendritic cells</label></div><hr />

### 12. How many interdisulphide linkage are found between two heavy chains?
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>a. 3</label><br />
    <input name="question12" type="radio" />
    <label>b. 4</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">c. 2</label><br />
    <input name="question12" type="radio" />
    <label>d. 1</label></div><hr />

### 13. Choose the incorrect statement.
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Everyone of us suffers from infectious diseases.</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">2. In typhoid, there is high fever recurring every 2-3 days.</label><br />
    <input name="question13" type="radio" />
    <label>3. Common cold infects respiratory passage.</label><br />
    <input name="question13" type="radio" />
    <label>4. Plague is bacterial disease.</label></div><hr />

### 14. Dysentery is a:
  <div className="tb-mcq">
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">1. Bacterial disease</label><br />
    <input name="question14" type="radio" />
    <label>2. Viral disease</label><br />
    <input name="question14" type="radio" />
    <label>3. Protozoan disease</label><br />
    <input name="question14" type="radio" />
    <label>4. Fungal disease</label></div><hr />

### 15. A disease that man has been fighting since years is:
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Cancer</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">2. Malaria</label><br />
    <input name="question15" type="radio" />
    <label>3. Dengue</label><br />
    <input name="question15" type="radio" />
    <label>4. AIDS</label></div><hr />

### 16. Select the correct statements among the following.
A. Each antibody molecule has four peptide chains.  
B. Anopheles mosquito is parasite on human being.  
C. Sporozoites escape from gut and migrate to salivary glands of mosquitoes.
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. Only C</label><br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">2. Both A and C</label><br />
    <input name="question16" type="radio" />
    <label>3. Both A and B</label><br />
    <input name="question16" type="radio" />
    <label>4. All A, B, C</label></div><hr />

### 17. Infectious agents transmitted through food and water such as:
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. Typhoid</label><br />
    <input name="question17" type="radio" />
    <label>2. Amoebiasis</label><br />
    <input name="question17" type="radio" />
    <label>3. Ascariasis</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">4. All of these</label></div><hr />

### 18. Most important method to control vectors is:
  <div className="tb-mcq">
    <input name="question18" type="radio" />
    <label>1. Avoiding contact with infected person</label><br />
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">2. Avoiding stagnation of water</label><br />
    <input name="question18" type="radio" />
    <label>3. Consumption of clean drinking water</label><br />
    <input name="question18" type="radio" />
    <label>4. Maintaining personal hygiene</label></div><hr />

### 19. State True or False
A. The discovery of blood by Harvey by pure reflective thought.  
B. Using thermometer Harvey proved the ‘good humor’ hypothesis.
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. T,F</label><br />
    <input name="question19" type="radio" />
    <label>2. F,T</label><br />
    <input name="question19" type="radio" />
    <label>3. T,T</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">4. F,F</label></div><hr />

### 20. Main barrier which prevents entry of microbes is
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1. Mucus coating</label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">2. Skin</label><br />
    <input name="question20" type="radio" />
    <label>3. Macrophage</label><br />
    <input name="question20" type="radio" />
    <label>4. Neutrophils</label></div><hr />

### 21. Choose the incorrect statement.
  <div className="tb-mcq">
    <input name="question21" type="radio" />
    <label>1. There is always a time lag between the infection and appearance of AIDS symptoms.</label><br />
    <input name="question21" type="radio" />
    <label>2. HIV spreads only through body fluids.</label><br />
    <input id="q21" name="question21" type="radio" />
    <label htmlFor="q21">3. After getting into body of person, virus enters into helper T cells where it replicates its RNA genome.</label><br />
    <input name="question21" type="radio" />
    <label>4. Anti-retroviral drugs cannot prevent death.</label></div><hr />

### 22. Choose the odd one with respect to cancer of body part caused by tobacco smoking.
  <div className="tb-mcq">
    <input name="question22" type="radio" />
    <label>1. Urinary bladder</label><br />
    <input name="question22" type="radio" />
    <label>2. Lung</label><br />
    <input id="q22" name="question22" type="radio" />
    <label htmlFor="q22">3. Oral cavity </label><br />
    <input name="question22" type="radio" />
    <label>4. Throat</label></div><hr />

### 23. Choose the odd one with respect to side effect caused by anabolic steroids in females.
  <div className="tb-mcq">
    <input name="question23" type="radio" />
    <label>1. Increased aggressiveness</label><br />
    <input id="q23" name="question23" type="radio" />
    <label htmlFor="q23">2. Breast enlargement</label><br />
    <input name="question23" type="radio" />
    <label>3. Masculinisation</label><br />
    <input name="question23" type="radio" />
    <label>4. Excessive hairs on face</label></div><hr />

### 24. Identify the given figure, flower responsible for hallucinogenic properties.
![Identify the given figure, flower responsible for hallucinogenic properties.](https://icdn.talentbrick.com/mcq/q5a6s.png)
  <div className="tb-mcq">
    <input name="question24" type="radio" />
    <label>1. Atropa bellodona</label><br />
    <input id="q24" name="question24" type="radio" />
    <label htmlFor="q24">2. Datura</label><br />
    <input name="question24" type="radio" />
    <label>3. Crack</label><br />
    <input name="question24" type="radio" />
    <label>4. Morphine</label></div><hr />

### 25. Majority of abused drugs are obtained from:
  <div className="tb-mcq">
    <input name="question25" type="radio" />
    <label>1. Fungi</label><br />
    <input id="q25" name="question25" type="radio" />
    <label htmlFor="q25">2. Flowering plants</label><br />
    <input name="question25" type="radio" />
    <label>3. Animals</label><br />
    <input name="question25" type="radio" />
    <label>4. Bacteria</label></div><hr />

### 26. Most cancers are treated by:
  <div className="tb-mcq">
    <input name="question26" type="radio" />
    <label>1. Surgery</label><br />
    <input name="question26" type="radio" />
    <label>2. Radiotherapy</label><br />
    <input name="question26" type="radio" />
    <label>3. Chemotherapy</label><br />
    <input id="q26" name="question26" type="radio" />
    <label htmlFor="q26">4. All of these</label></div><hr />

### 27. Heroin acts as:
  <div className="tb-mcq">
    <input name="question27" type="radio" />
    <label>1. Sedative</label><br />
    <input id="q27" name="question27" type="radio" />
    <label htmlFor="q27">2. Depressant</label><br />
    <input name="question27" type="radio" />
    <label>3. Hallucinogen</label><br />
    <input name="question27" type="radio" />
    <label>4. Stimulant</label></div><hr />

### 28. Choose the incorrect match:
  <div className="tb-mcq">
    <input name="question28" type="radio" />
    <label>1. Benign cancer – causes little damage</label><br />
    <input id="q28" name="question28" type="radio" />
    <label htmlFor="q28">2. Viral genes – c-onc</label><br />
    <input name="question28" type="radio" />
    <label>3. MRI – uses strong magnetic fields</label><br />
    <input name="question28" type="radio" />
    <label>4. Cancer drug – side effect is Anemia</label></div>