---
sidebar_position: 14
title: MCQ Questions Biomolecules Organic Chemistry Chapter 14 Class 12
description: Biomolecules Chapter 14 NCERT MCQ questions for Class 12. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/class-12-chem-chapter-14.png
sidebar_label: Chapter 14
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

NCERT Organic Chemistry Class 12 MCQ Practice Questions for Jee Mains | NEET | School Exams.

Prepare these important MCQ Questions of Biomolecules in organic chemistry, Latest questions to expect in Jee Mains | NEET | School Exams.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. Most common oligosaccharides are:
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Trisaccharides</label><br />
    <input name="question1" type="radio" />
    <label>2. Monosaccharides</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">3. Disaccharides</label> <br />
    <input name="question1" type="radio" />
    <label>4. Tetrasaccharides</label>
  </div><hr />

### 2. Find incorrect Statement.
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Reducing sugar, reduces Fehling’s reagent as well as Tollen’s reagent.</label><br />
    <input name="question2" type="radio" />
    <label>2. The most common sugar, used in our homes is sucrose.</label><br />
    <input name="question2" type="radio" />
    <label>3. Glucose occurs in free as well as combined state in nature.</label> <br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">4. Ribose is five carbon ketose sugar.</label>
  </div><hr />

### 3. Pyran and Pyrone are respectively:
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. Both Aromatic</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">2. Non-aromatic and Aromatic</label><br />
    <input name="question3" type="radio" />
    <label>3. Antiaromatic</label> <br />
    <input name="question3" type="radio" />
    <label>4. Aromatic and Non-aromatic</label>
  </div><hr />

### 4. Most commonly encountered carbohydrates in nature:
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. Monosaccharides</label><br />
    <input name="question4" type="radio" />
    <label>2. Disaccharides</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">3. Polysaccharides</label> <br />
    <input name="question4" type="radio" />
    <label>4. Trisaccharides</label>
  </div><hr />

### 5. Find the incorrect statement regarding proteins:
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. They occur in every part of our body.</label><br />
    <input name="question5" type="radio" />
    <label>2. They are required for growth of body.</label><br />
    <input name="question5" type="radio" />
    <label>3. All proteins are polymers of α-amino acids.</label> <br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">4. Word ‘Protein’ is derived from latin word, ‘proteios’.</label>
  </div><hr />

### 6. State True or False (T/F):
a. Protein found in biological system with a unique 3-D structure is called native protein.  
b. Almost all enzymes are globular proteins.  
c. All amino-acids have L-configurations.
  <div className="tb-mcq">
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">1. TTF</label><br />
    <input name="question6" type="radio" />
    <label>2. FTF</label><br />
    <input name="question6" type="radio" />
    <label>3. TTT</label> <br />
    <input name="question6" type="radio" />
    <label>4. TFT</label>
  </div><hr />

### 7. Vitamins are considered essential food factors because:
  <div className="tb-mcq">
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">1. Our body cannot synthesize most of vitamins.</label><br />
    <input name="question7" type="radio" />
    <label>2. They are vital amines which are not synthesized in our body.</label><br />
    <input name="question7" type="radio" />
    <label>3. They are required by bacteria of our gut.</label> <br />
    <input name="question7" type="radio" />
    <label>4. Both (1) &amp; (2)</label>
  </div><hr />

### 8. Increased fragility of RBC’s is due to deficiency of which Vitamin:
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>a. Vit B₁₂</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">b. Vit E</label><br />
    <input name="question8" type="radio" />
    <label>c. Vit B₉</label> <br />
    <input name="question8" type="radio" />
    <label>d. Vit C</label>
  </div><hr />

### 9. Which of the following doesn’t form Zwitterion.
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Sulphanilic acid</label><br />
    <input name="question9" type="radio" />
    <label>2. Glutamic acid</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">3. P-amino benzoic acid</label> <br />
    <input name="question9" type="radio" />
    <label>4. Glycine</label>
  </div><hr />

### 10. Read the following statements & choose the correct answer:
a. DNA fingerprinting is preffered over conventional fingerprinting.  
b. DNA is same for every cell and cannot be altered by any known treatment.
  <div className="tb-mcq">
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">1. Both (A) &amp; (B) are correct and (B) is correct reason of (A).</label><br />
    <input name="question10" type="radio" />
    <label>2. Both (A) &amp; (B) are correct and (B) is not correct reason of (A).</label><br />
    <input name="question10" type="radio" />
    <label>3. Only A is correct.</label> <br />
    <input name="question10" type="radio" />
    <label>4. Both (A) &amp; (B) are incorrect.</label>
  </div><hr />

### 11. Select the incorrect match:
  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. Glycine : Sweet taste.</label><br />
    <input name="question11" type="radio" />
    <label>2. Lysine : 6 – Carbon Amino acid.</label><br />
    <input name="question11" type="radio" />
    <label>3. Aspartic acid : Most acidic amino acid.</label> <br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">4. Glutamine : Represented by ‘E’.</label>
  </div><hr />

### 12. Histidine consists of:
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. Indole ring</label><br />
    <input name="question12" type="radio" />
    <label>2. Furan ring</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">3. Imidiazole ring</label> <br />
    <input name="question12" type="radio" />
    <label>4. Pyran ring</label>
  </div><hr />

### 13. Positive Ceric Ammonium nitrate test is given by:
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Serine</label><br />
    <input name="question13" type="radio" />
    <label>2. Methionine</label><br />
    <input name="question13" type="radio" />
    <label>3. Threonine</label> <br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">4. Both (1) &amp; (3)</label>
  </div><hr />

### 14. Most basic amino acid is:
  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>1. Ornithine</label><br />
    <input name="question14" type="radio" />
    <label>2. Lysine</label><br />
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">3. Arginine</label> <br />
    <input name="question14" type="radio" />
    <label>4. Histidine</label>
  </div><hr />

### 15. Major form of glucose in solution form is:
  <div className="tb-mcq">
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">1. β – glucose.</label><br />
    <input name="question15" type="radio" />
    <label>2. α – glucose.</label><br />
    <input name="question15" type="radio" />
    <label>3. Open Chain structure.</label> <br />
    <input name="question15" type="radio" />
    <label>4. Both α &amp; β in equal proportion.</label>
  </div><hr />

### 16. Choose the correct difference between Saccharic acid and Saccharin.
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. Both contain equal no. of carbon atoms.</label><br />
    <input name="question16" type="radio" />
    <label>2. Both are acyclic compounds.</label><br />
    <input name="question16" type="radio" />
    <label>3. Both are derived from D – glucose.</label> <br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">4. Both of them have at least 2 oxygen atoms.</label>
  </div><hr />

### 17. Rhamnose, a carbohydrate has formula:
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. C₆H₁₀O₆</label><br />
    <input name="question17" type="radio" />
    <label>2. C₆H₁₂O₆</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">3. C₆H₁₂O₅</label> <br />
    <input name="question17" type="radio" />
    <label>4. C₅H₁₂O₆</label>
  </div><hr />

### 18. What is the product P in the reaction:
[![](https://1.bp.blogspot.com/-W9z-VjkRGko/X4Acja8KEfI/AAAAAAAAAYU/zC6Ex6xsEwMuwcuBSTAqzarSHQGotI4bQCLcBGAsYHQ/s320/Organic%2BChemistry%2BBiomolecules%2BQ18.png)](https://1.bp.blogspot.com/-W9z-VjkRGko/X4Acja8KEfI/AAAAAAAAAYU/zC6Ex6xsEwMuwcuBSTAqzarSHQGotI4bQCLcBGAsYHQ/Organic%2BChemistry%2BBiomolecules%2BQ18.png)
  <div className="tb-mcq">
    <input name="question18" type="radio" />
    <label>1. Xanthine</label><br />
    <input name="question18" type="radio" />
    <label>2. Thymine</label><br />
    <input name="question18" type="radio" />
    <label>3. Cytosine</label> <br />
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">4. Uracil</label>
  </div><hr />

### 19. Which of the following sugar is found in RNA.
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. α – Ribose</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">2. β – Ribose</label><br />
    <input name="question19" type="radio" />
    <label>3. α – Deoxyribose</label> <br />
    <input name="question19" type="radio" />
    <label>4. Openchain Ribose</label>
  </div><hr />

### 20. How many of the following amino-acids have more than one nitrogen atom?
  <div className="tb-mcq">
<table><tbody><tr><td>Alanine, Histidine, Proline, Tyrosine, Tryptophan, Lysine.</td></tr></tbody></table>
    <input name="question20" type="radio" />
    <label>a. 2</label><br />
    <input name="question20" type="radio" />
    <label>b. 5</label><br />
    <input name="question20" type="radio" />
    <label>c. 4</label> <br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">d. 3</label>
  </div>