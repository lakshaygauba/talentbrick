---
sidebar_position: 8
title: MCQ Questions D and F Block Elements Inorganic Chemistry Chapter 8 Class 12
description: D-and-F Block Elements Inorganic Chemistry NCERT MCQ questions for Class 12. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/class-12-chem-chapter-8.png
sidebar_label: Chapter 8
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

NCERT Inorganic Chemistry D-and-F Block Elements Class 12th Chemistry MCQ Practice Questions for Jee Mains | NEET | Class 12.

Prepare these important Questions of D-and-F Block Elements in Inorganic Chemistry, Latest questions to expect in Jee Mains | NEET | School Exams.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. Copernicium has electronic configuration as:
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>a. 7s² 6d²</label><br />
    <input name="question1" type="radio" />
    <label>b. 7s² 6d⁴</label><br />
    <input name="question1" type="radio" />
    <label>c. 7s² 6d⁶</label> <br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">d. 7s² 6d¹⁰</label>
  </div><hr />

### 2. Arrange according to melting point order:
  <div className="tb-mcq">
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">1. W &gt; Re &gt; Ru &gt; Rh</label><br />
    <input name="question2" type="radio" />
    <label>2. Re &gt; Ru &gt; Rh &gt; W</label><br />
    <input name="question2" type="radio" />
    <label>3. Re &gt; W &gt;Ru &gt; Rh</label> <br />
    <input name="question2" type="radio" />
    <label>4.W &gt; Rh &gt; Ru &gt; Re</label>
  </div><hr />

### 3. Enthalpy of atomisation is maximum for:
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. Titanium</label><br />
    <input name="question3" type="radio" />
    <label>2. Cobalt</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">3. Vanadium</label> <br />
    <input name="question3" type="radio" />
    <label>4. Scandium</label>
  </div><hr />

### 4. Among Ti, V, Cr, Mn which metal has minimum metallic radius:
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. Ti</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">2. Cr</label><br />
    <input name="question4" type="radio" />
    <label>3. Mn</label> <br />
    <input name="question4" type="radio" />
    <label>4. V</label>
  </div><hr />

### 5. Which of the following element have highest oxidising potential in +3 oxidation state?
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Mn</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">2. Co</label><br />
    <input name="question5" type="radio" />
    <label>3. Fe</label> <br />
    <input name="question5" type="radio" />
    <label>4. V</label>
  </div><hr />

### 6. +5 oxidation state is not possesed by:
  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. Mn</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">2. Fe</label><br />
    <input name="question6" type="radio" />
    <label>3. Cr</label> <br />
    <input name="question6" type="radio" />
    <label>4. V</label>
  </div><hr />

### 7. The value of E⁰ is more negative than expected for:
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. Zn</label><br />
    <input name="question7" type="radio" />
    <label>2. Ni</label><br />
    <input name="question7" type="radio" />
    <label>3. Mn</label> <br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">4. All of these</label>
  </div><hr />

### 8. High value of E⁰ of Nickel is contributed to:
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1.Low ionisation enthalpy I</label><br />
    <input name="question8" type="radio" />
    <label>2. Low atomisation energy </label><br />
    <input name="question8" type="radio" />
    <label>3. Low ionisation enthalpy II</label> <br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">4. High hydration energy</label>
  </div><hr />

### 9.In VX₂ and CuX type halide X cannot be:
  <div className="tb-mcq">
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">1. F</label><br />
    <input name="question9" type="radio" />
    <label>2. Cl</label><br />
    <input name="question9" type="radio" />
    <label>3. Br</label> <br />
    <input name="question9" type="radio" />
    <label>4. I</label>
  </div><hr />

### 10. Ferrates (VI), are formed in:
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Acidic medium</label><br />
    <input name="question10" type="radio" />
    <label>2.Neutral medium</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">3. Alkaline medium</label> <br />
    <input name="question10" type="radio" />
    <label>4. Both 1 and 2</label>
  </div><hr />

### 11. Which among the following will release hydrogen gas on reaction with dilute acid?
  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. Mn⁺³</label><br />
    <input name="question11" type="radio" />
    <label>2. Co⁺³</label><br />
    <input name="question11" type="radio" />
    <label>3. Ti</label> <br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">4. Cr⁺²</label>
  </div><hr />

### 12. Wilkinson’s catalyst is:
  <div className="tb-mcq">
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">1. Homogeneous catalyst</label><br />
    <input name="question12" type="radio" />
    <label>2. Heterogeneous catalyst</label><br />
    <input name="question12" type="radio" />
    <label>3. Dehydrogenating catalyst</label> <br />
    <input name="question12" type="radio" />
    <label>4. Oxidising catalyst</label>
  </div><hr />

### 13.Which metal/ion catalyses reaction between iodine and persulphate ions?
  <div className="tb-mcq">
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">1. Fe(III)</label><br />
    <input name="question13" type="radio" />
    <label>2. Co(II)</label><br />
    <input name="question13" type="radio" />
    <label>3.Fe(II)</label> <br />
    <input name="question13" type="radio" />
    <label>4. Ni(II)</label>
  </div><hr />

### 14. Alloys are formed by atoms with metallic radii that are how much percent of each other?
  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>a. 20%</label><br />
    <input name="question14" type="radio" />
    <label>b. 5%</label><br />
    <input name="question14" type="radio" />
    <label>c. 50%</label> <br />
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">d. 15%</label>
  </div><hr />

### 15. Mn₂O₇ is a:
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Ionic green solid </label><br />
    <input name="question15" type="radio" />
    <label>2. Ionic yellow solid </label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">3. Covalent green oil</label> <br />
    <input name="question15" type="radio" />
    <label>4. Covalent yellow gas</label>
  </div><hr />

### 16.Potasium dichromate is used as —– in volumetric analysis
  <div className="tb-mcq">
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">1. Primary standard</label><br />
    <input name="question16" type="radio" />
    <label>2. Secondary standard</label><br />
    <input name="question16" type="radio" />
    <label>3. Indicator&nbsp;</label> <br />
    <input name="question16" type="radio" />
    <label>4. Catalyst</label>
  </div><hr />

### 17. In neutral medium thiosulphate is oxidised to which species by permanganate
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. Peroxodisulphuric acid</label><br />
    <input name="question17" type="radio" />
    <label>2. Caro acid</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">3.Sulphate</label> <br />
    <input name="question17" type="radio" />
    <label>4. Dithionate</label>
  </div><hr />

### 18.Lanthanum belongs to which block:
  <div className="tb-mcq">
    <input name="question18" type="radio" />
    <label>1. p </label><br />
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">2. d</label><br />
    <input name="question18" type="radio" />
    <label>3. s</label> <br />
    <input name="question18" type="radio" />
    <label>4. f</label>
  </div><hr />

### 19. Which of the following carbide is not formed by lanthanoids?
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>a. Ln₃C</label><br />
    <input name="question19" type="radio" />
    <label>b. Ln₂C₃</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">c. Ln₄C₃</label> <br />
    <input name="question19" type="radio" />
    <label>d. All of these</label>
  </div><hr />

### 20. What is configuration of Gd²⁺
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1. 4f⁸</label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">2. 4f⁷5d¹</label><br />
    <input name="question20" type="radio" />
    <label>3. 4f⁵5d³</label> <br />
    <input name="question20" type="radio" />
    <label>4. 4f⁷6s¹</label>
  </div><hr />

### 21. Which one of the following is coinage metal:
  <div className="tb-mcq">
    <input name="question21" type="radio" />
    <label>1. Ni</label><br />
    <input name="question21" type="radio" />
    <label>2. Fe</label><br />
    <input id="q21" name="question21" type="radio" />
    <label htmlFor="q21">3. Cu</label> <br />
    <input name="question21" type="radio" />
    <label>4. Zn</label>
  </div><hr />

### 22. Which of the following lanthanoid is steal hard?
  <div className="tb-mcq">
    <input name="question22" type="radio" />
    <label>1. Dy</label><br />
    <input id="q22" name="question22" type="radio" />
    <label htmlFor="q22">2. Sm</label><br />
    <input name="question22" type="radio" />
    <label>3. Ho</label> <br />
    <input name="question22" type="radio" />
    <label>4. Yb</label>
  </div><hr />

### 23. Which element is not present in misch metal:
  <div className="tb-mcq">
    <input name="question23" type="radio" />
    <label>1. Carbon</label><br />
    <input id="q23" name="question23" type="radio" />
    <label htmlFor="q23">2. Magnesium</label><br />
    <input name="question23" type="radio" />
    <label>3. Calcium</label> <br />
    <input name="question23" type="radio" />
    <label>4. Sulphur</label>
  </div><hr />

### 24. State True or False:
a. Hydroxides of lanthanoids are just hydrated oxides.  
b. Trivalent lanthanoids ions are colored in both solid state and aqueous solution.
  <div className="tb-mcq">
    <input name="question24" type="radio" />
    <label>1. F,F</label><br />
    <input name="question24" type="radio" />
    <label>2. T,F</label><br />
    <input name="question24" type="radio" />
    <label>3. T,T</label> <br />
    <input id="q24" name="question24" type="radio" />
    <label htmlFor="q24">4. F,T</label>
  </div><hr />

### 25. Actinoids on reaction with boiling water forms:
  <div className="tb-mcq">
    <input name="question25" type="radio" />
    <label>1. Oxides only</label><br />
    <input name="question25" type="radio" />
    <label>2. Hydride only</label><br />
    <input name="question25" type="radio" />
    <label>3. Hydroxides</label> <br />
    <input id="q25" name="question25" type="radio" />
    <label htmlFor="q25">4. Both Oxide and Hydride</label>
  </div><hr />

### 26. Choose the correct statement:
  <div className="tb-mcq">
    <input name="question26" type="radio" />
    <label>1. V₂O₄ do not dissolve in acids.</label><br />
    <input name="question26" type="radio" />
    <label>2. V₂O₄ dissolve in acids and form VO⁺ salts.</label><br />
    <input id="q26" name="question26" type="radio" />
    <label htmlFor="q26">3. V₂ O₄ dissolve in acids and form VO⁺² salts.</label> <br />
    <input name="question26" type="radio" />
    <label>4. V₂O₄ dissolve in acids and form VO₂⁺ salts.</label>
  </div><hr />

### 27. Choose the incorrect option with respect to uses:
  <div className="tb-mcq">
    <input name="question27" type="radio" />
    <label>1. MnO₂ – Dry Cells</label><br />
    <input name="question27" type="radio" />
    <label>2. Ln₂O₃ – Petroleum Cracking</label><br />
    <input name="question27" type="radio" />
    <label>3. AgBr – Photographic industry</label> <br />
    <input id="q27" name="question27" type="radio" />
    <label htmlFor="q27">4. V₂O₅ – Pigment industry</label>
  </div><hr />

### 28. Which of the following do not exhibit +4 oxidation states?
  <div className="tb-mcq">
    <input name="question28" type="radio" />
    <label>1. Pr</label><br />
    <input id="q28" name="question28" type="radio" />
    <label htmlFor="q28">2. Sm</label><br />
    <input name="question28" type="radio" />
    <label>3. Nd</label> <br />
    <input name="question28" type="radio" />
    <label>4. Tb</label>
  </div><hr />

### 29. In the reaction of Fe⁺² ions with potassium permanganate, color of solution changes from:
  <div className="tb-mcq">
    <input name="question29" type="radio" />
    <label>1. Yellow to Green</label><br />
    <input name="question29" type="radio" />
    <label>2. Red to Yellow</label><br />
    <input name="question29" type="radio" />
    <label>3. Violet to Green</label> <br />
    <input id="q29" name="question29" type="radio" />
    <label htmlFor="q29">4. Green to Yellow</label>
  </div><hr />

### 30. Which of the following for manganate ion is true:
  <div className="tb-mcq">
    <input name="question30" type="radio" />
    <label>1. Diamagnetism only</label><br />
    <input name="question30" type="radio" />
    <label>2. Diamagnetism along with temperature dependent strong paramagnetism</label><br />
    <input name="question30" type="radio" />
    <label>3. Diamagnetism along with temperature independent weak paramagnetism</label> <br />
    <input id="q30" name="question30" type="radio" />
    <label htmlFor="q30">4. Diamagnetism along with temperature dependent weak paramagnetism</label>
  </div>