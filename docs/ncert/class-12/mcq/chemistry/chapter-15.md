---
sidebar_position: 15
title: MCQ Questions for Class 12 Chemistry Chapter 15 Polymers with Answers
description: Polymers Chapter 15 NCERT MCQ questions for Class 12. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/class-12-chem-chapter-15.png
sidebar_label: Chapter 15
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

MCQ Questions for Class 12 Chemistry Chapter 15 Polymers for NEET |  School Exams | Class 12

Prepare these important MCQ Questions of Class 12 Chemistry Chapter 15 Polymers with Answers, These are Latest questions to expect in NEET | School Exams.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. Which one of the following is natural polymer:
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Cellulose acetate</label><br />
    <input name="question1" type="radio" />
    <label>2. Glyptal</label><br />
    <input name="question1" type="radio" />
    <label>3. Cellulose nitrate</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">4. Resins</label></div><hr />

### 2. Choose the correct statement.
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Dacron is synthesized by using acid or basic catalysis.</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">2. Novolac requires heating to form Bakelite.</label><br />
    <input name="question2" type="radio" />
    <label>3. Copolymers can be made step growth polymerization only.</label><br />
    <input name="question2" type="radio" />
    <label>4. Melamine formaldehyde is used making combs.</label></div><hr />

### 3. Which of the following has the superior resistance to vegetable and mineral oils?
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. Buna-N</label><br />
    <input name="question3" type="radio" />
    <label>2. Buna-S</label><br />
    <input name="question3" type="radio" />
    <label>3. Natural rubber</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">4. Neoprene</label></div><hr />

### 4. Which of the following polymer is used as Insulator?
  <div className="tb-mcq">
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">1. Polystyrene</label><br />
    <input name="question4" type="radio" />
    <label>2. Polypropene</label><br />
    <input name="question4" type="radio" />
    <label>3. Polyethene&nbsp;</label><br />
    <input name="question4" type="radio" />
    <label>4. Bakelite</label></div><hr />

### 5. Which is the most important class of biodegradable polymer?
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Aromatic</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">2. Aliphatic</label><br />
    <input name="question5" type="radio" />
    <label>3. Cyclic</label><br />
    <input name="question5" type="radio" />
    <label>4. Oxolanes</label></div><hr />

### 6. Which of the following is fiber forming solid?
  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. Nylon – 2,6</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">2. Nylon – 6</label><br />
    <input name="question6" type="radio" />
    <label>3. PHBV</label><br />
    <input name="question6" type="radio" />
    <label>4. Dacron</label></div><hr />

### 7. Match the polymers given in Column I with their main applications given in Column II.
[![](https://1.bp.blogspot.com/-U_ozQd09bk8/YGBxgxNPjvI/AAAAAAAAArk/o0qsFJxLcpE4-rYJ2Vk9WXF4UE6ZgpSYwCLcBGAsYHQ/s0/NCERT%2BQUESTION.png)](https://1.bp.blogspot.com/-U_ozQd09bk8/YGBxgxNPjvI/AAAAAAAAArk/o0qsFJxLcpE4-rYJ2Vk9WXF4UE6ZgpSYwCLcBGAsYHQ/s0/NCERT%2BQUESTION.png)
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. i = a, ii = c, iii = d, iv = e, v = b, vi = f</label><br />
    <input name="question7" type="radio" />
    <label>2. i = a, ii = c, iii = f, iv = b, v = b, vi = f</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">3. i = d, ii = e, iii = a, iv = f, v = b, vi = c</label><br />
    <input name="question7" type="radio" />
    <label>4. i = e, ii = e, iii = c, iv = f, v = b, vi = a</label></div><hr />

### 8. Polymers property is closely related to
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. Size</label><br />
    <input name="question8" type="radio" />
    <label>2. Molecular mass</label><br />
    <input name="question8" type="radio" />
    <label>3. Structure</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">4. All of these</label></div><hr />

### 9. For which type of rubber 5% of Sulphur is used at crosslinking agent?
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Cable insulation rubber</label><br />
    <input name="question9" type="radio" />
    <label>2. Pipes rubber</label><br />
    <input name="question9" type="radio" />
    <label>3. Glass rubber</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">4. Tyre rubber</label></div><hr />

### 10. Which of the following is not true about low density polythene?
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Tough</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">2. Hard</label><br />
    <input name="question10" type="radio" />
    <label>3. Poor conductor of Electricity</label><br />
    <input name="question10" type="radio" />
    <label>4. Highly branched structure</label></div><hr />

### 11. Which of the following is not a semisynthetic polymer?
  <div className="tb-mcq">
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">1. cis-polyisoprene</label><br />
    <input name="question11" type="radio" />
    <label>2. Cellulose nitrate</label><br />
    <input name="question11" type="radio" />
    <label>3. Cellulose accetate</label><br />
    <input name="question11" type="radio" />
    <label>4. Vulcanised rubber</label></div><hr />

### 12. Oldest synthetic polymers.
  <div className="tb-mcq">
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">a. Phenol formaldehyde</label><br />
    <input name="question12" type="radio" />
    <label>b. Urea formaldehyde</label><br />
    <input name="question12" type="radio" />
    <label>c. Nylon – 6</label><br />
    <input name="question12" type="radio" />
    <label>d. Rayon</label></div><hr />

### 13. Polymers used glass reinforcing material.
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Nylon – 6</label><br />
    <input name="question13" type="radio" />
    <label>2. Phenol formaldehyde</label><br />
    <input name="question13" type="radio" />
    <label>3. Rayon</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">4. Dacron</label></div><hr />

### 14. Zinc acetate antimony oxide is used as catalyst for.
  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>1. Nylon-6</label><br />
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">2. Dacron</label><br />
    <input name="question14" type="radio" />
    <label>3. Nylon-6,6</label><br />
    <input name="question14" type="radio" />
    <label>4. Buna-N</label></div>