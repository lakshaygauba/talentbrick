---
sidebar_position: 13
title: MCQ Questions Amines Class 12 Chemistry Chapter 13
description: Amines Chemistry Chapter 13 NCERT MCQ questions for Class 12. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/class-12-chem-chapter-13.png
sidebar_label: Chapter 13
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

NCERT Chemistry Class 12 MCQ Practice Questions Organic Chemistry Amines Chapter 13 for JEE | NEET | Class 12. Prepare these important [MCQ Questions of Chemistry Class 12](/ncert/class-12/mcq/chemistry) Amines Chapter 13, Latest questions to expect in NEET | JEE | School Exams.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. Choose the correct IUPAC name of Ph-NH₂

  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Aniline</label><br />
    <input name="question1" type="radio" />
    <label>2. Benzenamine</label><br />
    <input name="question1" type="radio" />
    <label>3. Amino Benzene</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">4. Both 1 and 2</label>
  </div><hr />

### 2. How many of the following contains secondary amino groups?

| Adrenaline, Surfactants, Benadryl, Ephedrine |
| -------------------------------------------- |


  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>a. 1</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">b. 2</label><br />
    <input name="question2" type="radio" />
    <label>c. 3</label><br />
    <input name="question2" type="radio" />
    <label>d. 4</label>
  </div><hr />

### 3. The major products A and B for following reactions are respectively:

![](https://icdn.talentbrick.com/mcq/eq365.png)

  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1.<img src="https://icdn.talentbrick.com/mcq/B-r.png" /></label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">2.<img src="https://icdn.talentbrick.com/mcq/A-r.png" /></label><br />
    <input name="question3" type="radio" />
    <label>3.<img src="https://icdn.talentbrick.com/mcq/C-r.png" /></label><br />
    <input name="question3" type="radio" />
    <label>4.<img src="https://icdn.talentbrick.com/mcq/D-r.png" /></label>
  </div><hr />

### 4. What is the name of the H₂N–CH₂–CH₂–NH₂

  <div className="tb-mcq">
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">1. ethane-1, 2-diamine</label><br />
    <input name="question4" type="radio" />
    <label>2. ethana-1, 2-diamine</label><br />
    <input name="question4" type="radio" />
    <label>3. ethylene diamine</label><br />
    <input name="question4" type="radio" />
    <label>4. ethylidene amine</label>
  </div><hr />

### 5. Reaction of Aniline with sulphuric acid at room temperature gives

  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Ortho-Subsituted Zwitterion</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">2. Anilinium Salt</label><br />
    <input name="question5" type="radio" />
    <label>3. Para-Substituted Zwitterion</label><br />
    <input name="question5" type="radio" />
    <label>4. Meta-Subsituted Zwitterion</label>
  </div><hr />

### 6. With which of the following reagents reduction of nitro group is preffered?

  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. SnCl₄</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">2. FeCl₂</label><br />
    <input name="question6" type="radio" />
    <label>3. FeCl₃</label><br />
    <input name="question6" type="radio" />
    <label>4. Sn</label>
  </div><hr />

### 7. Primary amines with more than \_\_ carbons are liquid.

  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>a. 9</label><br />
    <input name="question7" type="radio" />
    <label>b. 2</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">c. 3</label><br />
    <input name="question7" type="radio" />
    <label>d. 4</label>
  </div><hr />

### 8. Choose the correct statement.

  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. Arylamines are usually colored but get decolorises due to reduction.</label><br />
    <input name="question8" type="radio" />
    <label>2. Arylamines are usually colorless but get colored due to reduction.</label><br />
    <input name="question8" type="radio" />
    <label>3. Arylamines are usually colored but get decolorises due to oxidation.</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">4. Arylamines are usually colorless but get colored due to oxidation.</label>
  </div><hr />

### 9. Arrange the compounds of similar molecular mass on the basis of decreasing boiling point.

  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Tertiarry Amine, Alcohol, Primary Amine, Secondary Amine</label><br />
    <input name="question9" type="radio" />
    <label>2. Primary Amine, Secondary Amine, Tertiarry Amine, Alcohol</label><br />
    <input name="question9" type="radio" />
    <label>3. Primary Amine, Tertiarry Amine, Alcohol, Secondary Amine</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">4. Alcohol, Primary Amine, Secondary Amine, Tertiarry Amine</label>
  </div><hr />

### 10. Aliphatic Amines have the pKb value range.

  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. 8 – 9.2</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">2. 3 – 4.2</label><br />
    <input name="question10" type="radio" />
    <label>3. 5 – 6.2</label><br />
    <input name="question10" type="radio" />
    <label>4. 1.2 – 2</label>
  </div><hr />

### 11. Ammonia on reaction with carboxylic acid at room temperature forms.

  <div className="tb-mcq">
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">1. Salt</label><br />
    <input name="question11" type="radio" />
    <label>2. Amide</label><br />
    <input name="question11" type="radio" />
    <label>3. Imine</label><br />
    <input name="question11" type="radio" />
    <label>4. Nitrile</label>
  </div><hr />

### 12. Arrange the following in increasing order of solubility in water (C₂H₅)₂NH, C₂H₅NH₂, C₆H₅NH₂

  <div className="tb-mcq">
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">1. C₆H₅NH₂, (C₂H₅)₂NH, C₂H₅NH₂</label><br />
    <input name="question12" type="radio" />
    <label>2. (C₂H₅)₂NH, C₂H₅NH₂, C₆H₅NH₂</label><br />
    <input name="question12" type="radio" />
    <label>3. (C₂H₅)₂NH, C₆H₅NH₂, C₂H₅NH₂</label><br />
    <input name="question12" type="radio" />
    <label>4. C₂H₅NH₂, (C₂H₅)₂NH, C₆H₅NH₂</label>
  </div><hr />

### 13. Which of the following statement is true for diazonium fluoroborate?

  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Soluble in water</label><br />
    <input name="question13" type="radio" />
    <label>2. On heating gives inorganic graphite</label><br />
    <input name="question13" type="radio" />
    <label>3. On reaction with NaNO₂ to give fluorobenzene</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">4. Stable at room temperature</label>
  </div><hr />

### 14. Arrange in the increasing order of Basic nature in water.

  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>1. Methanamine, N,N-Diethylethanamine, Ethanamine</label><br />
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">2. Methanamine, Ethanamine, N,N-Diethylethanamine</label><br />
    <input name="question14" type="radio" />
    <label>3. Ethanamine, N,N-Diethylethanamine, Methanamine</label><br />
    <input name="question14" type="radio" />
    <label>4. N,N-Diethylethanamine, Methanamine, Ethanamine</label>
  </div><hr />

### 15. The major product of the following reaction is

  <div className="tb-mcq">
    <div className="separator" style={{"clear":"both","margin-bottom":"12px"}}><a href="https://1.bp.blogspot.com/-7egyd9AOsWo/YIa_m91fbbI/AAAAAAAAAwU/6O83jzL_afY3qyRd5IieCsR3vCOI8dWOwCLcBGAsYHQ/s0/Talentbrick.com%2Bmajor%2Bquestion.png" style={{"display":"block","padding":"1em 0","-webkit-text-align":"center","text-align":"center", "background":"white"}}><img alt="The major product of the following reaction is" data-original-height={169} data-original-width={541} src="https://1.bp.blogspot.com/-7egyd9AOsWo/YIa_m91fbbI/AAAAAAAAAwU/6O83jzL_afY3qyRd5IieCsR3vCOI8dWOwCLcBGAsYHQ/s0/Talentbrick.com%2Bmajor%2Bquestion.png" border={0} /></a>
    </div>
    <img alt data-original-height={253} data-original-width={240} style={{"background":"white"}} src="https://1.bp.blogspot.com/-bB0VcFNJ8NU/YIbA3xzO2GI/AAAAAAAAAwc/1co143uMgcwlbB_yf_KFcvCBoKAH4nihgCLcBGAsYHQ/s0/question-a.png" border="2px" /><img style={{"background":"white"}} alt data-original-height={253} data-original-width={146} src="https://1.bp.blogspot.com/-C9z5OmnDgqw/YIbA3-5qTvI/AAAAAAAAAwk/J3YpO3t77LYWgEwZjNgsW9cRwKtAzczSgCLcBGAsYHQ/s0/question-b.png" style={{"background":"white"}} border="2px" /><img alt data-original-height={253} data-original-width={145} src="https://1.bp.blogspot.com/-FE1ENVpn1Rw/YIbA30sz1gI/AAAAAAAAAwg/X3l6pTwHLAYqOn_L7AEICKOkZVijzDE2wCLcBGAsYHQ/s0/question-c.png" style={{"background":"white"}} border="2px" /><img alt data-original-height={253} data-original-width={208} src="https://1.bp.blogspot.com/-NNi9561_8Ao/YIbA4YaB4mI/AAAAAAAAAwo/Wiiini02ntMhqxR_Us4n2Kv-sD6zLrF5QCLcBGAsYHQ/s0/question-d.png" style={{"background":"white"}} border="2px" />
    <input name="question15" type="radio" />
    <label>1. A</label><br />
    <input name="question15" type="radio" />
    <label>2. B</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">3. C</label><br />
    <input name="question15" type="radio" />
    <label>4. D</label>
  </div>
