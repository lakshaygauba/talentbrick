---
sidebar_position: 7
title: MCQ Questions P Block Elements Inorganic Chemistry Chapter 7 Class 12
description: P Block Elements Inorganic Chemistry | NCERT Physics MCQ questions for Class 12. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/class-12-chem-chapter-7.png
sidebar_label: Chapter 7
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

NCERT Inorganic Chemistry Class – 12 MCQ Practice Questions for Jee Mains | NEET | Class 12

Prepare these Important MCQ Questions of Chapter 7 p-Block Elements in Inorganic Chemistry, Latest questions to expect in Jee Mains | NEET | School Exams.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. N-N bond is weaker than P-P bond due to repulsion of __
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Anti bonding electrons</label>
    <br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">2. Non bonding electrons</label>
    <br />
    <input name="question1" type="radio" />
    <label>3. Bonding electrons</label>
    <br />
    <input name="question1" type="radio" />
    <label>4. None</label>
  </div><hr />

### 2. Which among the following has smallest ionic radius?
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. P</label>
    <br />
    <input name="question2" type="radio" />
    <label>2. As</label>
    <br />
    <input name="question2" type="radio" />
    <label>3. N</label>
    <br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">4. Sb</label>
  </div><hr />

### 3. At very high temperature dinitrogen combines with dioxygen to form __
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. NO₂</label>
    <br />
    <input name="question3" type="radio" />
    <label>2. N₂O₅</label>
    <br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">3. NO</label> <br />
    <input name="question3" type="radio" />
    <label>4. N₂O₄</label>
  </div><hr />

### 4. Which of the following is ionic fluoride?
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. SbF₃</label>
    <br />
    <input name="question4" type="radio" />
    <label>2. AsF₃</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">3. BiF₃</label> <br />
    <input name="question4" type="radio" />
    <label>4. Both 1 and 3</label>
  </div><hr />

### 5. How many group 15 hydrides have enthalpy of formation negative?
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>a. 2</label>
    <br />
    <input name="question5" type="radio" />
    <label>b. 3</label>
    <br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">c. 1</label>
    <br />
    <input name="question5" type="radio" />
    <label>d. 0</label>
  </div><hr />

### 6. Order of third ionisation of group 15 elements is
  <div className="tb-mcq">
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">1. N &gt; P &gt; As &gt; Bi &gt; Sb</label>
    <br />
    <input name="question6" type="radio" />
    <label>2. N &gt; P &gt; As &gt; Sb &gt; Bi</label>
    <br />
    <input name="question6" type="radio" />
    <label>3. N &gt; As &gt; Sb &gt; Bi &gt; P</label>
    <br />
    <input name="question6" type="radio" />
    <label>4. P &gt; As &gt; Sb &gt; Bi &gt; N</label>
  </div><hr />

### 7. The correct order of melting point is:
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. Water &gt; Hydrogen Fluoride &gt; Ammonia</label>
    <br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">2. Water &gt; Ammonia &gt; Hydrogen Fluoride</label>
    <br />
    <input name="question7" type="radio" />
    <label>3. Hydrogen Fluoride &gt; Water &gt; Ammonia</label>
    <br />
    <input name="question7" type="radio" />
    <label>4. Ammonia &gt; Water &gt; Hydrogen Fluoride</label>
  </div><hr />

### 8. The Maximum covalency of oxygen can be __
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>a. 2</label>
    <br />
    <input name="question8" type="radio" />
    <label>b. 3</label>
    <br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">c. 4</label>
    <br />
    <input name="question8" type="radio" />
    <label>d. 1</label>
  </div><hr />

### 9. Choose the correct statement among the following:
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. All group 16 elements except oxygen show allotropy.</label><br />
    <input name="question9" type="radio" />
    <label>2. All group 15 elements show allotropy.</label><br />
    <input name="question9" type="radio" />
    <label>3. All halogens except Astatine are colored. </label> <br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">4. All group 15 elements are polymorphic in nature.</label>
  </div><hr />

### 10. Dinitrogen pentoxide is prepared by treating:
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Nitric acid with NO.</label><br />
    <input name="question10" type="radio" />
    <label>2. Thermal decomposition of Lead nitrate.</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">3. Nitric acid with P4O10.</label> <br />
    <input name="question10" type="radio" />
    <label>4. All of the above</label>
  </div><hr />

### 11. Which of the following acid is used in manufacture of glusose from corn starch?
  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. HNO₃</label><br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">2. HCl</label><br />
    <input name="question11" type="radio" />
    <label>3. H₂SO₄</label> <br />
    <input name="question11" type="radio" />
    <label>4. H₃PO₄</label>
  </div><hr />

### 12. Select the Correct Statement:
  <div className="tb-mcq">
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">1. Mustard gas have 4 carbon atoms present in its structure.</label><br />
    <input name="question12" type="radio" />
    <label>2. Platinum forms coordination number 4 compound, when dissolved in aqua regia.</label><br />
    <input name="question12" type="radio" />
    <label>3. At high temperature(823K) NaCl reacts with sulphuric acid to form sodium hydrogen sulphate. </label>
    <br />
    <input name="question12" type="radio" />
    <label>4. All of these.</label>
  </div><hr />

### 13. Which among the following property of fluorine is less than expected?
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Ionisation Enthalpy</label>
    <br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">2. Melting point and Boiling point</label>
    <br />
    <input name="question13" type="radio" />
    <label>3. Melting point and Electronegativity</label>
    <br />
    <input name="question13" type="radio" />
    <label>4. Covalent radius and Electrode potential.</label>
  </div><hr />

### 14. S8 reacts with chlorine to form X. Which of the following is correct for X.
  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>1. It has distorted tetrahedron shape.</label><br />
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">2. Sulphur has +1 oxidation state in X.</label>
    <br />
    <input name="question14" type="radio" />
    <label>3. X is cyclic structure similar to benzene.</label> <br />
    <input name="question14" type="radio" />
    <label>4. There is no S-S bond present in X.</label>
  </div><hr />

### 15. Bromine reacts with excess flourine to form compound Y. Y has shape of _
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Square Planar</label>
    <br />
    <input name="question15" type="radio" />
    <label>2. Bent T</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">3. Square Pyramidal</label> <br />
    <input name="question15" type="radio" />
    <label>4. Pentagonal Bipyramidal</label>
  </div><hr />

### 16. Arrange the following in order of their electron gain enthalpy.
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. He &gt; Ne &gt; Ar &gt; Kr &gt; Xe &gt; Rn</label>
    <br />
    <input name="question16" type="radio" />
    <label>2. He &gt; Kr &gt; Xe &gt; Ne &gt; Ar &gt; Rn</label><br />
    <input name="question16" type="radio" />
    <label>3. Ne &gt; Ar &gt; Kr &gt; Xe &gt; He &gt; Rn</label> <br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">4. Ne &gt; Ar &gt; Kr &gt; Xe &gt; Rn &gt; He</label>
  </div><hr />

### 17. Choose the correct statements.
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. HCl is used for extracting glue from bones.</label>
    <br />
    <input name="question17" type="radio" />
    <label>2. Oxygen is used in manufacture of many metals, particularly steel.</label><br />
    <input name="question17" type="radio" />
    <label>3. Potassium Permanganate is used in production of ozone.</label>
    <br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">4. Both 1 and 2.</label>
  </div><hr />

### 18. Iodine + excess chlorine reacts to give compound __
  <div className="tb-mcq">
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">1. Orange Solid</label><br />
    <input name="question18" type="radio" />
    <label>2. Black Solid </label>
    <br />
    <input name="question18" type="radio" />
    <label>3. Colorless Gas</label>
    <br />
    <input name="question18" type="radio" />
    <label>4. Ruby Red Solid</label>
  </div><hr />

### 19. Which one is most abundant noble gas in atmosphere?
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. Ne</label><br />
    <input name="question19" type="radio" />
    <label>2. He</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">3. Ar</label>
    <br />
    <input name="question19" type="radio" />
    <label>4. Kr</label>
  </div><hr />

### 20. What is color of O₂PtF₆ and XePtF₆ respectively?
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1. Red, Blue</label>
    <br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">2. Both Red</label>
    <br />
    <input name="question20" type="radio" />
    <label>3. Red, Colorless</label>
    <br />
    <input name="question20" type="radio" />
    <label>4. Colorless, Red</label>
  </div><hr />

### 21. Choose the odd one out with respect to state of compound (solid, liquid, gas)
  <div className="tb-mcq">
    <input name="question21" type="radio" />
    <label>1. XeF₂</label>
    <br />
    <input name="question21" type="radio" />
    <label>2. XeO₃</label>
    <br />
    <input id="q21" name="question21" type="radio" />
    <label htmlFor="q21">3. XeOF₄</label>
    <br />
    <input name="question21" type="radio" />
    <label>4. XeF₆</label>
  </div><hr />

### 22. A sequence of reactions of phosphorous (P₄) is given below, the correct set of products (Q, R, S and T) among the following is
![](https://icdn.talentbrick.com/mcq/p-Block-MCQ-q22.png)
  <div className="tb-mcq">
    <input name="question22" type="radio" />
    <label>1. Q = PCl₃; R = POCl₃; S = P₂O₃; T = H₃PO₃</label>
    <br />
    <input name="question22" type="radio" />
    <label>2. Q = PCl₅; R = P₂O₅; S = P₄O₆; T = H₃PO₃</label>
    <br />
    <input id="q22" name="question22" type="radio" />
    <label htmlFor="q22">3. Q = PCl₃; R = POCl₃; S = P₄O₁₀; T = H₃PO₄</label>
    <br />
    <input name="question22" type="radio" />
    <label>4. Q = PCl₅; R = P₄O₁₀; S = P₄O₁₀; T = H₃PO₄</label>
  </div><hr />

### 23. Choose the correct statement.
  <div className="tb-mcq">
    <input id="q23" name="question23" type="radio" />
    <label htmlFor="q23">1. All group 16 elements form hydrides of type H₂E.</label><br />
    <input name="question23" type="radio" />
    <label>2. All hexafluorides of group 16 elements are in liquid state.</label><br />
    <input name="question23" type="radio" />
    <label>3. DIchloride of selenium disproportionate to form compounds of +6 and -2 oxidation state. </label>
    <br />
    <input name="question23" type="radio" />
    <label>4. SeO₂ is found in liquid state.</label>
  </div><hr />

### 24. Which of the following metals occurs in nature s decay product of uranium and thorium?
  <div className="tb-mcq">
    <input name="question24" type="radio" />
    <label>1. Livermorium</label><br />
    <input id="q24" name="question24" type="radio" />
    <label htmlFor="q24">2. Polonium</label>
    <br />
    <input name="question24" type="radio" />
    <label>3. Moscovium</label> <br />
    <input name="question24" type="radio" />
    <label>4. Oganesson</label>
  </div><hr />

### 25. Bond angle in ozone is:
  <div className="tb-mcq">
    <input name="question25" type="radio" />
    <label>a. 135</label><br />
    <input name="question25" type="radio" />
    <label>b. 120</label><br />
    <input name="question25" type="radio" />
    <label>c. 107</label> <br />
    <input id="q25" name="question25" type="radio" />
    <label htmlFor="q25">d. 117</label>
  </div><hr />

### 26. Cyclo-S6 ring adopts which form
  <div className="tb-mcq">
    <input name="question26" type="radio" />
    <label>1. Half chair form</label><br />
    <input name="question26" type="radio" />
    <label>2. Twisted form</label><br />
    <input name="question26" type="radio" />
    <label>3. Boat form</label> <br />
    <input id="q26" name="question26" type="radio" />
    <label htmlFor="q26">4. Chair form</label>
  </div><hr />

### 27. Choose the incorrect statement.
  <div className="tb-mcq">
    <input id="q27" name="question27" type="radio" />
    <label htmlFor="q27">1. Bond length in cyclo-S6 form is less than that of S8.</label><br />
    <input name="question27" type="radio" />
    <label>2. Monoclinic sulhphur has higher MP than that of rhombic sulphur.</label><br />
    <input name="question27" type="radio" />
    <label>3. Monoclinic sulhur has lower specific gravity than that of rhombic sulphur.</label> <br />
    <input name="question27" type="radio" />
    <label>4. Bond angle is greater in S8 than in cyclo-S6.</label>
  </div><hr />

### 28. Chalcogen is greek word for
  <div className="tb-mcq">
    <input name="question28" type="radio" />
    <label>1. Copper</label>
    <br />
    <input name="question28" type="radio" />
    <label>2. Oxygen</label>
    <br />
    <input name="question28" type="radio" />
    <label>3. Sulphur</label>
    <br />
    <input id="q28" name="question28" type="radio" />
    <label htmlFor="q28">4. Brass</label>
  </div><hr />

### 29. Which Interhalogen is so unstable that it has been just spectroscopically detected?
  <div className="tb-mcq">
    <input id="q29" name="question29" type="radio" />
    <label htmlFor="q29">1. IF</label>
    <br />
    <input name="question29" type="radio" />
    <label>2. IBr</label><br />
    <input name="question29" type="radio" />
    <label>3. ClF₃</label> <br />
    <input name="question29" type="radio" />
    <label>4. BrF₅</label>
  </div><hr />

### 30. Identify the nitrogen compounds A, B, C, D, and E.
![](https://icdn.talentbrick.com/mcq/p-Block-MCQ-q30.png)
  <div className="tb-mcq">
    <input name="question30" type="radio" />
    <label>1. A = NO₂ and C = NO₂</label>
    <br />
    <input name="question30" type="radio" />
    <label>2. B = N₂O₄ and D = N₂O₃</label>
    <br />
    <input id="q30" name="question30" type="radio" />
    <label htmlFor="q30">3. C = NO and E = NO₂</label>
    <br />
    <input name="question30" type="radio" />
    <label>4. A = NO₂, B = N₂O₃</label>
  </div>