---
sidebar_position: 11
title: MCQ Questions Dual Nature of Radiation and Matter Physics Class 12 Chapter 11
description: Dual Nature of Radiation and Matter | NCERT Physics MCQ questions for Class 12. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/class-12-phy-chapter-11.png
sidebar_label: Chapter 11
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

NCERT Physics – Dual Nature of Radiation and Matter Class 12 Practice Questions for Jee Mains | NEET | School Exams.

Prepare these important Questions of Nature of Matter and Radiation, Latest questions to expect in Jee Mains | NEET | School Exams.

<div className="hide-print-56">

:::info
Scroll down and select an option in the questions and know the answer or click on the Show answers button below to toggle all the answers.
<button className="button button--success show-ans-7">Show Answers</button>
<button style={{"display":"none"}} className="button button--warning hide-ans-5">Hide Answers</button>
:::

</div>

### 1. Cathode Rays were discovered by:
  <div className="tb-mcq">
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">1. William Crookes</label><br />
    <input name="question1" type="radio" />
    <label>2. J.J Thompson</label><br />
    <input name="question1" type="radio" />
    <label>3. Roentgen</label> <br />
    <input name="question1" type="radio" />
    <label>4. R.A Mulliken</label>
  </div><hr />

### 2. Choose the correct one:
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Work function is generally measured in joules.</label><br />
    <input name="question2" type="radio" />
    <label>2. Work function is not much sensitive to surface impurities.</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">3. Work function is maximum for Al among Na, Ca, Al.</label> <br />
    <input name="question2" type="radio" />
    <label>4. Alkali metals responds only to UV light.</label>
  </div><hr />

### 3. When charged zinc plate is illuminated by UV light, then leaves of electroscope will:
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. Repel only.</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">2. Collapse and then further repel.</label><br />
    <input name="question3" type="radio" />
    <label>3. Collapse and then attract.</label> <br />
    <input name="question3" type="radio" />
    <label>4. Attract and repel continiously.</label>
  </div><hr />

### 4. Who studied photo current variation with collector plate potential?
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. Albert Einstein.</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">2. Hallwachs and Lenard.</label><br />
    <input name="question4" type="radio" />
    <label>3. Henrich Hertz.</label> <br />
    <input name="question4" type="radio" />
    <label>4. Davisson &amp; Germer.</label>
  </div><hr />

### 5. Quartz window permits which light spectrum to pass through:
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Infrared only</label><br />
    <input name="question5" type="radio" />
    <label>2. Visible and infrared only</label><br />
    <input name="question5" type="radio" />
    <label>3. Microwaves and radiowaves only</label> <br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">4. UV rays</label>
  </div><hr />

### 6. Photoelectric current is measured by:
  <div className="tb-mcq">
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">1. Microammeter.</label><br />
    <input name="question6" type="radio" />
    <label>2. Galvanometer with high resistance in series.</label><br />
    <input name="question6" type="radio" />
    <label>3. Ammeter</label> <br />
    <input name="question6" type="radio" />
    <label>4. Milliammeter</label>
  </div><hr />

### 7. Estimate the speed with which electrons emitted from the heated emitter of an evacuated tube impinge on collector maintained at a potential difference of 20 MV. Ignore the small initial speed of electrons.
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>a. 2.65 x 10⁹ m/sec</label><br />
    <input name="question7" type="radio" />
    <label>b. 2.65 x 10⁸ m/sec</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">c. 2.99 x 10⁸ m/sec</label> <br />
    <input name="question7" type="radio" />
    <label>d. 2.96 x 10⁷ m/sec</label>
  </div><hr />

### 8. Why gases start conducting at low pressure?
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. They ionise rapidly due to decreased internal energy.</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">2. Ions have higher chance to reach electrode, as chance of recombination decreases.</label><br />
    <input name="question8" type="radio" />
    <label>3. Gas behavior becomes near to ideal gas behavior.</label><br />
    <input name="question8" type="radio" />
    <label>4. Gas expand at low pressure.</label></div><hr />

### 9. Compute the typical de-Broglie wavelength for electron in metal at 300K and compare it with mean seperation between two electrons which is about 2 Å. then
  <div className="tb-mcq">
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">1. de-Broglie wavelength is about 6 nm and there is strong overlap of electron wave packets.</label><br />
    <input name="question9" type="radio" />
    <label>2. de-Broglie wavelength is about 6 Å and there is strong overlap of electron wave packets.</label><br />
    <input name="question9" type="radio" />
    <label>3. de-Broglie wavelength is about 6 μm and there is no overlap of electron wave packets.</label><br />
    <input name="question9" type="radio" />
    <label>4. de-Broglie wavelength is about 60 Å and there is no overlap of electron wave packets.</label></div><hr />

### 10. Observable charges in nature are
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. +2/3 e</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">2. Integral multiple of e</label><br />
    <input name="question10" type="radio" />
    <label>3. -1/3 e</label><br />
    <input name="question10" type="radio" />
    <label>4. All of these</label></div><hr />

### 11. Choose the correct statement with respect to wavelength, velocity and frequency associated with matter wave.
  <div className="tb-mcq">
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">1. Both wavelength and group velocity are of physical significance.</label><br />
    <input name="question11" type="radio" />
    <label>2. Both phase velocity and group velocity are of physical significance.</label><br />
    <input name="question11" type="radio" />
    <label>3. Both phase velocity and particle velocity are of physical significance.</label><br />
    <input name="question11" type="radio" />
    <label>4. Both wavelength and phase velocity are of physical significance.</label></div><hr />

### 12. Which of the following is incorrect with respect to photoelectric effect?
![](https://1.bp.blogspot.com/-AcolMi_Akl0/X7FkD1G_NCI/AAAAAAAAAPg/NpfSpikHHP42DuRN5-e40V30tCNrIX0mwCLcBGAsYHQ/opt1.png)![](https://1.bp.blogspot.com/-NRopDfHckFI/X7FkJQQFAUI/AAAAAAAAAPk/vl7jROADfRQkvzoYiR-4-bCL34fTMm0FgCLcBGAsYHQ/opt2.png)
  <div className="tb-mcq">
      <input name="question12" type="radio" />
      <label>a. 1)</label><br />
      <input name="question12" type="radio" />
      <label>b. 2)</label><br />
      <input id="q12" name="question12" type="radio" />
      <label htmlFor="q12">c. 3)</label><br />
      <input name="question12" type="radio" />
      <label>d. 4)</label></div><hr />

### 13. Photoelectric current do not depends on:
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Intensity of light</label><br />
    <input name="question13" type="radio" />
    <label>2. Potential difference applied between electrode</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">3. Stopping potential</label><br />
    <input name="question13" type="radio" />
    <label>4. Nature of emitter</label></div><hr />

### 14. Interference fringes are observed for:
  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>1. Iodine molecules</label><br />
    <input name="question14" type="radio" />
    <label>2. Electrons</label><br />
    <input name="question14" type="radio" />
    <label>3. Neutrons</label><br />
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">4. All of these</label></div><hr />

### 15. Which scientist obtained accurate value of Planck’s constant h?
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Max Planck</label><br />
    <input name="question15" type="radio" />
    <label>2. Max Born</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">3. Mullikan</label><br />
    <input name="question15" type="radio" />
    <label>4. Thomson</label></div><hr />

### 16. Choose the correct statement.
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. A wave of definite wavelength is not extended all over space.</label><br />
    <input name="question16" type="radio" />
    <label>2. Matter wave is made of single wavelength</label><br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">3. de-Broglie relation and Born’s probability interpretation reproduce the Heisenberg principle exactly.</label><br />
    <input name="question16" type="radio" />
    <label>4. de-Broglie hypothesis does not support Bohr’s concept of stationary orbit.</label></div><hr />

### 17. Matter waves are
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. Electromagnetic waves</label><br />
    <input name="question17" type="radio" />
    <label>2. Mechanical waves</label><br />
    <input name="question17" type="radio" />
    <label>3. Both 1 and 2</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">4. Neither 1 nor 2</label></div><hr />

### 18. Electric eye converts:
  <div className="tb-mcq">
    <input name="question18" type="radio" />
    <label>1. Change in frequency of illumination into change in photocurrent.</label><br />
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">2. Change in intensity of illumination into change in photocurrent.</label><br />
    <input name="question18" type="radio" />
    <label>3. Change in frequency of illumination into change in intensity.</label><br />
    <input name="question18" type="radio" />
    <label>4. Change in photocurrent of illumination into change in frequency.</label></div><hr />

### 19. State true or false:
A. Absorption by rods and cones require wave picture of light.  
B. Gathering of light by eye-lens require wave picture of light.
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. T T</label><br />
    <input name="question19" type="radio" />
    <label>2. F T</label><br />
    <input name="question19" type="radio" />
    <label>3. T F</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">4. F T</label></div><hr />

### 20. Which of the following cannot be explained by wave model.
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1. Rectilinear propagation.</label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">2. Low temperature specific heat of solids.</label><br />
    <input name="question20" type="radio" />
    <label>3. Diffraction.</label><br />
    <input name="question20" type="radio" />
    <label>4. Reflection.</label></div>