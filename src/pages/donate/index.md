---
title: Donate
hide_table_of_contents: true
---
<div style={{textAlign: 'center' , padding: '25px' , border: '2px solid' , borderRadius: '15px'}}>
<h1>Why Donate to TalentBrick?</h1>

In this era of IT, information is what precisely keeps the world about and going. So on one hand, where all websites collect your data (and then subsequently sell it for a price), WE DON'T.

Also, we believe knowledge shouldn't be capitalised upon and hence DON'T RUN ADS on TalentBrick.

In addition to this, we work on an open-source model, i.e. the full source is publicly available on GitHub/GitLab. We work upon subscriber suggestions and try to keep pace with ever evolving methodologies. To maintain all these things we need to hire people at TalentBrick.

Our content is exclusive as we contact the achievers directly and there are no middlemen involved. 

In order to support this endeavour of ours, we have then no option left but to call upon you, our patrons, students and community members to support our website.

It doesn't have to be much. Only maybe once a month, contribution equivalent to a cup of coffee/tea (whatever you like) would suffice.

WE ARE THE CHANGE. COME JOIN US

<a class="button button--lg" style={{color: '#2196f2' , border: '3px solid' , borderRadius: '20px' , margin: '10px', fontSize: '23px'}} href="/donate/upi">
🇮🇳 UPI</a>
<a class="button button--lg" style={{ color: '#2196f2' , border: '3px solid' , borderRadius: '20px', margin: '10px'}} href="https://paypal.me/talentbrick"><img style={{width: '94px', marginTop: '5px'}} src="https://www.paypalobjects.com/digitalassets/c/website/logo/full-text/pp_fc_hl.svg"/></a>
</div>
