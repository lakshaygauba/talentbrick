import React from 'react';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import BlogLayout from '@theme/BlogLayout';
import BlogPostItem from '@theme/BlogPostItem';
import BlogListPaginator from '@theme/BlogListPaginator';
import {ThemeClassNames} from '@docusaurus/theme-common';
import Link from '@docusaurus/Link';

function BlogListPage(props) {
  const {metadata, items, sidebar} = props;
  const {
    siteConfig: {title: siteTitle},
  } = useDocusaurusContext();
  const {blogDescription, blogTitle, permalink} = metadata;
  const isBlogOnlyMode = permalink === '/';
  const title = isBlogOnlyMode ? siteTitle : blogTitle;
  return (
    <BlogLayout
      title={title}
      description={blogDescription}
      wrapperClassName={ThemeClassNames.wrapper.blogPages}
      pageClassName={ThemeClassNames.page.blogListPage}
      searchMetadata={{
        // assign unique search tag to exclude this page from search results!
        tag: 'blog_posts_list',
      }}
      sidebar={sidebar}>
      <div style={{ textAlign: 'center', padding: '25px'}}>
      <h1>TalentBrick Blog</h1>
      <p>Here you get experience from toppers, Exam updates, the Latest news, and much more.</p>
      <a class="button button--lg" style={{ color: '#2196f2', border: '3px solid', borderRadius: '20px', margin: '10px', fontSize: '20px' }} href="https://ask.talentbrick.com/contact-us">Write Blog 📝</a><hr/>
        <h2 style={{ marginBottom: '0px' }}>Filters</h2>
        <Link class="btn back" title="NEET Blog" to="/blog/tags/neet">NEET</Link>
        <Link class="btn back" title="NEET Blog" to="/blog/tags/jee">JEE</Link>
        <Link class="btn back" title="INBO Blog" to="/blog/tags/inbo">INBO</Link>
        <Link class="btn back" title="KVPY Blog" to="/blog/tags/kvpy">KVPY</Link>
        <Link class="btn back" title="NTSE Blog" to="/blog/tags/ntse">NTSE</Link>
        <Link class="btn back" title="TalentBrick Related" to="/blog/tags/talentbrick">TalentBrick Related</Link>
    </div>
      {items.map(({content: BlogPostContent}) => (
        <BlogPostItem
          key={BlogPostContent.metadata.permalink}
          frontMatter={BlogPostContent.frontMatter}
          assets={BlogPostContent.assets}
          metadata={BlogPostContent.metadata}
          truncated={BlogPostContent.metadata.truncated}>
          <BlogPostContent />
        </BlogPostItem>
      ))}
      <BlogListPaginator metadata={metadata} />
    </BlogLayout>
  );
}

export default BlogListPage;
