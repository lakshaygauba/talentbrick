import React, { useLayoutEffect } from 'react';
import { useLocation } from '@docusaurus/router';
import Layout from '@theme-original/Layout';

function useLocationChangeTracker() {
  const { pathname } = useLocation();
  useLayoutEffect(() => {
    if (['mcq', 'chapter'].every(seg => location.pathname.includes(seg))) {
      var labels = document.getElementsByTagName("label"); for (let a = 0; a < labels.length; a++)labels[a].addEventListener("click", addclass); function addclass(a) { a.target.classList.add("tb-wrng"); a.target.parentNode.classList.add("show-crctbox");  }
      function clearRadioGroup(GroupName) {
        var ele = document.getElementsByTagName(GroupName);
        for (var i = 0; i < ele.length; i++)
          ele[i].checked = false;
      }
      clearRadioGroup("input");
      document.querySelector('.show-ans-7').addEventListener('click', () => {
        document.querySelector('.show-ans-7').style.display = "none";
        document.querySelector('.hide-ans-5').style.display = "block";
        var xm, im;
        xm = document.querySelectorAll(".tb-mcq");
        for (im = 0; im < xm.length; im++) {
          xm[im].classList.add('showall-true');
        }
      });
      document.querySelector('.hide-ans-5').addEventListener('click', () => {
        document.querySelector('.hide-ans-5').style.display = "none";
        document.querySelector('.show-ans-7').style.display = "block";
        var xm, im;
        xm = document.querySelectorAll(".tb-mcq");
        for (im = 0; im < xm.length; im++) {
          xm[im].classList.remove('showall-true');
        }
        clearRadioGroup("input");
        var xms, ims;
        xms = document.querySelectorAll(".tb-mcq label");
        for (ims = 0; ims < xms.length; ims++) {
          xms[ims].classList.remove('tb-wrng');
        }

        var xmss, imss;
        xmss = document.querySelectorAll(".tb-mcq");
        for (imss = 0; imss < xmss.length; imss++) {
          xmss[imss].classList.remove('show-crctbox');
        }
      });
    }
  }, [pathname]);
}

function LayoutWrapper(props) {
  useLocationChangeTracker();
  return <Layout {...props} />;
}

export default LayoutWrapper;